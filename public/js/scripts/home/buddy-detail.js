$(function () {
    'use strict';

    var $userRating = $('.user-rating');

    if ($userRating.length) {
        var rating = $userRating.attr('data-rating');
        $userRating.rateYo({
          rating: rating,
          starWidth: "25px"
        });
      }
});