
function upgradeMember(){
  $.ajax({
    url: '/settings/upgrade',
    type: 'POST',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    contentType: 'application/json',
    dataType: 'json',
    data: [],
    success: function(response){
      toastr['success'](response.message, {
        timeOut: 1000
      });
    },
    error: function(response) {
      toastr['warning'](response.responseJSON.message, {
        timeOut: 1000
      });
    }
  });
}

function updatePassword(){

  var $obj = {};
  $obj["password"] = $("#password").val();
  $obj["password_confirmation"] = $("#password-confirm").val();
  var $json = JSON.stringify($obj);

  $.ajax({
    url: '/settings/profile/update-password',
    type: 'POST',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    contentType: 'application/json',
    dataType: 'json',
    data: $json,
    success: function(response){
      $('#modal-password').modal('toggle');
      toastr['success'](response.message, {
        timeOut: 1000
      });
    },
    error: function(response) {
      toastr['warning'](response.responseJSON.message, {
        timeOut: 1000
      });
    }
  });
}

$(function () {
  'use strict';

  var $assetPath = '../../../app-assets/';

  if ($('body').attr('data-framework') === 'laravel') {
    $assetPath = $('body').attr('data-asset-path');
  }

  var $dzProfile = $('.dzProfile');
  var $dzGallery = $('.dzGallery');

  var $token = "{!! csrf_token() !!}";
  $dzProfile.autoDiscover = false;
  $dzGallery.autoDiscover = false;

  $dzProfile.dropzone({
    url: "settings/profile/update-image",
    paramName: "file",
    maxFilesize: 0.5,
    addRemoveLinks: true,
    acceptedFiles: "image/*",
    clickable: '#select-files-profile',
    params: {
        _token: $token
    },
    init: function() {
        this.on("error", function(file, message) {
          toastr['warning'](message, {
            timeOut: 1000
          });
        })
        this.on("addedfile", function(file) {
          console.log("Added file.");
        }),
        this.on("success", function(file, response) {
          location.reload();
        })
    }
  });

  $dzGallery.dropzone({
    url: "settings/gallery/add",
    paramName: "file",
    maxFilesize: 0.5,
    addRemoveLinks: true,
    acceptedFiles: "image/*",
    clickable: '#select-files-gallery',
    params: {
        _token: $token
    },
    init: function() {
        this.on("error", function(file, message) {
          toastr['warning'](message, {
            timeOut: 1000
          });
        })
        this.on("addedfile", function(file) {
          console.log("Added file.");
        }),
        this.on("success", function(file, response) {
          location.reload();
        })
    }
  });

  var $btnUpgrade = $('.btn-upgrade-member');
  $btnUpgrade.on('click', function(e){
    e.preventDefault();
    upgradeMember();
  });

  var $btnUpdatePassword = $('.btn-update-password');
  $btnUpdatePassword.on('click', function(e){
    e.preventDefault();
    updatePassword();
  });

});