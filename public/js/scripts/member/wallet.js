$(function () {
    'use strict';

    var $rangePickr = $('.flatpickr-range'),
    $dataType = $('.data-type'),
    $navMain = $('.navigation-main'),
    $assetPath = '../../../app-assets/';

    if ($dataType.length) {
      var userType = parseInt($dataType.attr('data-user-type'));

      if (userType == 0){
        $navMain.find('.nav-item:nth-child(3)').remove();
      } 
      
      if (userType == 1){
        $navMain.find('.nav-item:nth-child(8)').remove();
      } 
      
    }
    
    if ($rangePickr.length) {
      $rangePickr.flatpickr({
        mode: 'range'
      });
    }

    if ($('body').attr('data-framework') === 'laravel') {
      $assetPath = $('body').attr('data-asset-path');
      console.log($assetPath);
    }

});