function updateService($id) {
  var $scheduleList = $('.schedule-list');
  var $activity = $('.input-activity').select2().val();
  var $price = $('.input-price').select2().val();
  var $participant = $('.input-participant').val();

  var $totalSchedule = $scheduleList.children();

  var $obj = {};
  $obj["activity"] = $activity;
  $obj["price"] = $price;
  $obj["participant"] = $participant;

  var $schedules = [];

  for (let i = 0; i < $totalSchedule.length; i++) {

    var $schedule = {};
    var $days = [];

    $scheduleList.children().eq(i).find("input:checkbox[name=days]").each(function(){
      if($(this).is(':checked')){
        $days.push($(this).val());
      }
    });

    $schedule["id"] = $scheduleList.children().eq(i).attr('data-schedule');
    $schedule["days"] = $days;
    $schedule["start_time"] = $scheduleList.children().eq(i).find(".input-start-time").select2().val();
    $schedule["end_time"] = $scheduleList.children().eq(i).find(".input-end-time").select2().val();

    $schedules.push($schedule);
    
  }
  
  $obj["schedules"] = $schedules;

  var $json = JSON.stringify($obj);

  $.ajax({
      url: '/settings/services/' + $id + '/update',
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      contentType: 'application/json',
      dataType: 'json',
      data: $json,
      success: function(response){
        window.location.replace("/settings");
      }
  });
}

function deleteService($id) {
  $.ajax({
    url: '/settings/services/' + $id + '/delete',
    type: 'POST',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    contentType: 'application/json',
    dataType: 'json',
    success: function(response){
      window.location.replace("/settings");
    }
  });
}

function addSchedule() {
  var $inputSchedule = $('.input-schedule');
  var $scheduleList = $('.schedule-list');

  if ($scheduleList.children().length < 5){
    $inputSchedule.find('.select2.form-control').select2("destroy").end();
    $inputSchedule.children().clone().appendTo('.schedule-list');

    $scheduleList.find('.select2.form-control').each(function () {
      var $this = $(this);
      $this.wrap('<div class="position-relative"></div>');
      $this.select2({
        dropdownAutoWidth: true,
        dropdownParent: $this.parent(),
        width: '100%',
        containerCssClass: 'select-md'
      });
    });

    $inputSchedule.find('.select2.form-control').select2();
  }
}

function deleteSchedule($id) {
  if ($id != 0){
    $.ajax({
      url: '/settings/services/schedules/' + $id + "/delete",
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      contentType: 'application/json',
      dataType: 'json',
      success: function(response){
        toastr['success'](response.responseJSON.message, {
          timeOut: 1000
        });
      }
    });
  } 
}

$(function () {
  'use strict';
  
  var 
    $btnAddSchedule = $('.btn-add-schedule'),
    $btnSaveService = $('.btn-save-service'),
    $btnDeleteService = $('.btn-delete-service');

  $btnAddSchedule.on('click', function(e){
    e.preventDefault();
    addSchedule();
  })
  
  $(document).on('click', '.btn-delete-schedule', function (e) {
    e.preventDefault();
    $(this).closest('.card').remove();
    var $id = $(this).closest('.card').attr('data-schedule');
    if ($id != 0) {
      deleteSchedule($id)
    } else {
      toastr['success']('Schedule deleted', {
        timeOut: 1000
      });
    }
  });

  $btnSaveService.on('click', function(e){
    e.preventDefault();
    var $id = $(this).attr('data-service');
    updateService($id);
  });

  $btnDeleteService.on('click', function(e){
    e.preventDefault();
    var $id = $(this).attr('data-service');
    deleteService($id);
  });

});