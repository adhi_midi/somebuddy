<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserWishlist extends Model
{
    use HasFactory;
    protected $table = 'user_wishlist';

    protected $fillable = [
        'user_id',
        'service_is'
    ];
}
