<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserWithdrawRequest extends Model
{
    use HasFactory;
    protected $table = 'user_withdraw_request';

    protected $fillable = [
        'user_id',
        'balance_request',
        'status',
        'finish_at',
    ];
}
