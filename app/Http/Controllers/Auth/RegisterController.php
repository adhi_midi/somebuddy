<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\UserImageIdentity;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone_number' => ['required', 'string', 'max:14', 'unique:users'],
            'address' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'integer', 'max:1'],
            // 'image_identity' => ['required', 'image'],
            // 'image_selfie' => ['required', 'image'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        $request = app('request');
        $imageIdentityName = '';
        $imageSelfieName = '';

        if ($request->hasfile('image_identity')) {
            $imageIdentity = $request->file('image_identity');
            $imageIdentityName = time() . '_id.' . $imageIdentity->getClientOriginalExtension();

            Image::make($imageIdentity)->resize(300, 300)->save(public_path('images/user_images_identity/' . $imageIdentityName));
        }

        if ($request->hasfile('image_selfie')) {
            $imageSelfie = $request->file('image_selfie');
            $imageSelfieName = time() . '_selfie.' . $imageSelfie->getClientOriginalExtension();

            Image::make($imageSelfie)->resize(300, 300)->save(public_path('images/user_images_identity/' . $imageSelfieName));
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone_number' => $data['phone_number'],
            'address' => $data['address'],
            'gender' => $data['gender'],
            'image_identity' => $imageIdentityName,
            'image_selfie' => $imageSelfieName,
            'image_profile' => 'default-avatar.png',
            'user_type' => 0,
        ]);

        $user->assignRole('member');

        return $user;

    }

    // Register
    public function showRegistrationForm()
    {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/auth/register', [
            'pageConfigs' => $pageConfigs
        ]);
    }
}
