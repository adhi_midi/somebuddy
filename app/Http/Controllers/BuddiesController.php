<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserImageGallery;
use App\Models\UserService;
use App\Models\UserServiceSchedule;
use App\Models\UserReview;
use App\Models\UserWishlist;
use App\Models\Master\Gender;
use App\Models\Master\Language;
use App\Models\Master\Nationality;
use App\Models\Master\City;
use App\Models\Master\Activity;
use App\Models\Master\Interest;
use App\Models\Master\Price;
use App\Models\Front\HeroBanner;
use App\Models\Front\Banner;
use App\Models\Front\CallToAction;
use App\Models\Front\SocialMedia;
use Illuminate\Support\Facades\Log;
use App\Services\HomeService;
use App\Services\BuddyService;

class BuddiesController extends Controller
{

  protected $service, $homeService;
  
  public function __construct(
    BuddyService $service, 
    HomeService $homeService
    )
  {
    $this->service = $service;
    $this->homeService = $homeService;
  }

  public function index(){

    $pageConfigs = ['showMenu' => false];
    
    $cities = City::get()->all();
    $activities = Activity::get()->all();

    $heroBanner = $this->service->getHeroBanner();
    $banners = $this->homeService->getBanner();
    $callToAction = $this->homeService->getCallToAction();
    $socialMedia = $this->homeService->getSocailMedia();

    $services = $this->service->getServices();

    return view('/content/buddy/index', ['pageConfigs'=>$pageConfigs, 'heroBanner'=>$heroBanner, 'banners'=>$banners, 'callToAction'=>$callToAction, 'socialMedia'=>$socialMedia, 'cities'=>$cities, 'activities'=>$activities, 'services'=>$services]);
  }

  public function detail($id){

    $pageConfigs = ['showMenu' => false];

    $profile = $this->service->getUserDetail($id);
    $services = $this->service->getUserServices($id);
    $galleries = $this->service->getGalleries($id);
    $reviews = $this->service->getReviews($id);

    $heroBanner = $this->homeService->getHeroBanner();
    $callToAction = $this->homeService->getCallToAction();
    $socialMedia = $this->homeService->getSocailMedia();

    return view('/content/buddy/detail', ['pageConfigs'=>$pageConfigs, 'profile'=>$profile, 'reviews'=>$reviews, 'galleries'=>$galleries, 'services'=>$services, 'heroBanner'=>$heroBanner, 'callToAction'=>$callToAction, 'socialMedia'=>$socialMedia]);
  }

}
