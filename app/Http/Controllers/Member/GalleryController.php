<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserImageGallery;
use Carbon\Carbon;
use Image;
use Illuminate\Support\Facades\Log;

class GalleryController extends Controller
{
  public function index(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    return view('/content/member/settings/gallery/index',['user'=>$user]);
  }

  public function store(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    $image = $request->file('file');
    $imageName = $userId.'_'.time().'.'.$image->extension();
    $image->move(public_path('images/gallery/'),$imageName);
    
    $imageUpload = new UserImageGallery();
    $imageUpload->user_id = $user->id;
    $imageUpload->image = $imageName;
    $imageUpload->save();

    return response()->json(['success' => true, 'message' => 'Image Uploaded'], 200);
  }

  public function delete(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    return back()->with('success',"update successfully"); 
  }
}
