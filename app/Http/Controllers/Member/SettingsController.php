<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Master\Gender;
use App\Models\Master\Language;
use App\Models\Master\Nationality;
use App\Models\Master\City;
use App\Models\Master\Activity;
use App\Models\Master\Interest;
use App\Models\Master\Price;
use App\Models\UserImageGallery;
use App\Models\UserService;
use App\Models\UserServiceSchedule;
use App\Models\UserUpgradeRequest;
use Carbon\Carbon;
use Image;
use Illuminate\Support\Facades\Log;

class SettingsController extends Controller
{

  public function profile(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    $genders = Gender::get()->all();
    $languages = Language::get()->all();
    $nationalities = Nationality::get()->all();
    $cities = City::get()->all();
    $activities = Activity::get()->all();
    $interests = Interest::get()->all();
    $galleries = UserImageGallery::where('user_id', $userId)->get()->all();

    $userProfile = $this->getUserDetail($user, $languages, $activities, $interests);

    if (Auth::user()->role('buddy')){
      $userServices = $this->getUserServices($user);
    } else {
      $userServices = array();
    }

    return view('/content/member/settings/profile/index',['user'=>$user, 'profile'=>$userProfile, 'galleries'=>$galleries, 'services'=>$userServices, 'genders'=>$genders, 'languages'=>$languages, 'nationalities'=>$nationalities, 'cities'=>$cities, 'activities'=>$activities, 'interests'=>$interests]);
  }

  public function updateProfile(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    if($user) {
        $user->name = $request->name;
        $user->language = $request->language;
        $user->gender = $request->gender;
        $user->nationality = $request->nationality;
        $user->location = $request->location;
        $user->phone_number = $request->phone_number;
        $user->about = $request->about;
        $user->activity = $request->activity;
        $user->interest = $request->interest;
        if(!empty($request->birthdate)) {
          $user->birthdate = Carbon::createFromFormat('d-m-Y', $request->birthdate)->format('Y-m-d');
        }else{
          $user->birthdate = null;
        }
        
        $user->save();
    }

    return back()->with('success',"Update successfully"); 
  }

  public function updateProfileImage(Request $request)
  {
      $userId = Auth::user()->id;
      $user = User::findOrFail($userId);

      $image = $request->file('file');
      $imageName = $user->id.'_'.time().'.'.$image->extension();
      Image::make($image)->resize(500, 500)->save(public_path('images/profile/' . $imageName));
      Image::make($image)->resize(100, 100)->save(public_path('images/profile/thumb/' . $imageName));

      $user->image_profile = $imageName;
      $user->save();

      return response()->json(['success' => true, 'message' => 'Image Uploaded'], 200);
  }

  public function updatePassword(Request $request){

    $validator = Validator::make($request->all(), [
        'password' => ['required', 'string', 'min:8', 'confirmed']
    ]);

    if ($validator->fails()) {
      if ($validator->messages()->first() == "validation.min.string"){
        return response()->json(['success' => false, 'message' => 'Password must be more than 8 characters'], 400);
      }

      if ($validator->messages()->first() == "validation.confirmed"){
        return response()->json(['success' => false, 'message' => 'Password does not macth'], 400);
      }
    }

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    if($user) {
      $user->password = Hash::make($request->password);
      $user->save();
    }

    return response()->json(['success' => true, 'message' => 'Password updated'], 200);
  }

  public function getUserDetail($user, $languages, $activities, $interests){

    $profile = [];
    $profile['name'] = $user->name;
    $profile['image'] = $user->image_profile;
    $profile['birthdate'] = Carbon::parse($user->birthdate)->format('d M Y');
    $profile['phone'] = $user->phone_number;
    $profile['about'] = $user->about;
    $profile['gender'] = Gender::where('id', $user->gender)->get()->first()->name;

    if (!empty($user->nationality)){
      $profile['nationality'] = Nationality::where('id', $user->nationality)->get()->first()->name;
    } else {
      $profile['nationality'] = '';
    }

    if (!empty($user->nationality)){
      $profile['location'] = City::where('city_id', $user->location)->get()->first()->city_name;
    } else {
      $profile['location'] = '';
    }

    if (!empty($user->language)){
      $language = array();
      foreach ($languages as $data){
        if (in_array($data->id, array_map('intval', json_decode($user->language)))){
          array_push($language, $data->name);
        }
      }
      $profile['language'] = $language;
    } else {
      $profile['language'] = [];
    }

    if (!empty($user->activity)){
      $activity = array();
      foreach ($activities as $data){
        if (in_array($data->id, array_map('intval', json_decode($user->activity)))){
          array_push($activity, $data->name);
        }
      }
      $profile['activity'] = $activity;
    } else {
      $profile['activity'] = [];
    }

    if (!empty($user->interest)){
      $interest = array();
      foreach ($interests as $data){
        if (in_array($data->id, array_map('intval', json_decode($user->interest)))){
          array_push($interest, $data->name);
        }
      }
      $profile['interest'] = $interest;
    } else {
      $profile['interest'] = [];
    }
    
    return $profile;
  }

  public function getUserServices($user){

    $datas = array();

    $query = UserService::where('user_service.user_id', $user->id)
      ->where('user_service.status', 1)
      ->join(config('database.connections.mysql_master.database') . '.activities as t1', 't1.id', '=', 'user_service.activity_id')
      ->join(config('database.connections.mysql_master.database') . '.prices as t2', 't2.id', '=', 'user_service.price')
      ->get(['user_service.*', 't1.name as service_name', 't2.name as price']);

    foreach ($query as $item){ 

      $data = [];
      $data['service_id'] = $item->id;
      $data['activity'] = $item->service_name;
      $data['price'] = number_format($item->price, 0, ',', '.');
      $data['participant'] = $item->participant;
      $data['schedules'] = $this->getUserServiceSchedules($item->id);

      array_push($datas, $data);
      
    }

    return $datas;
  }

  public function getUserServiceSchedules($id){
    
    $schedules = array();
    $schedulesData = UserServiceSchedule::where('service_id', $id)->get()->all();

    foreach ($schedulesData as $data){ 

      $days = array();
      $daysData = array_map('intval', json_decode($data->days));

      foreach ($daysData as $day){
        switch($day) {
          case(1): 
            array_push($days, "Senin");
            break;
          case(2): 
            array_push($days, "Selasa");
            break;
          case(3): 
            array_push($days, "Rabu");
            break;
          case(4): 
            array_push($days, "Kamis");
            break;
          case(5): 
            array_push($days, "Jumat");
            break;
          case(6): 
            array_push($days, "Sabtu");
            break;
          case(7): 
            array_push($days, "Minggu");
            break;
        }
      }

      $scheduleData = [
        'days'  => $days,
        'start_time'  => Carbon::parse($data->start_time)->format('H:i'),
        'end_time'  => Carbon::parse($data->end_time)->format('H:i')
      ];

      array_push($schedules, $scheduleData);
      
    }

    return $schedules;
  }

  public function upgradeMember(){

    $userId = Auth::user()->id;
    $userUpgradeRequest = UserUpgradeRequest::where('user_id', $userId)->get()->first();

    if (!empty($userUpgradeRequest)){
      return response()->json(['success' => false, 'message' => 'Already have upgrade request'], 400);
    }

    $request = new UserUpgradeRequest;
    $request->user_id = $userId;
    $request->status = 0;
    $request->save();

    return response()->json(['success' => true, 'message' => 'Upgrate request submitted'], 200);

  }

  public function upgradeApprove($id){

    $request = UserUpgradeRequest::where('user_id', $id)->get()->first();

    if ($request->status == 0){
      $request->status = 1;
      $request->approved_at = Carbon::now();
      $request->save();
  
      $user = User::findOrFail($id);
      $user->user_type = 2;
      $user->save();
      $user->syncRoles('buddy');
      return response()->json(['success' => true, 'message' => 'Update User to Buddy'], 200);
    } else {
      $request->status = 0;
      $request->approved_at = Carbon::now();
      $request->save();
  
      $user = User::findOrFail($id);
      $user->user_type = 1;
      $user->save();
      $user->syncRoles('member');
      return response()->json(['success' => true, 'message' => 'Update User to Member'], 200);
    }

  }

}
