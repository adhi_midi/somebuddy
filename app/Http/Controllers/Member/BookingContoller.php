<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use App\Models\Front\HeroBanner;
use App\Models\Front\Banner;
use App\Models\Front\CallToAction;
use App\Models\Front\SocialMedia;
use App\Models\Master\Gender;
use App\Models\Master\Language;
use App\Models\Master\Nationality;
use App\Models\Master\City;
use App\Models\Master\Activity;
use App\Models\Master\Interest;
use App\Models\Master\Price;
use App\Models\Master\Bank;
use App\Models\Master\Payment;
use App\Models\Master\Time;
use App\Models\User;
use App\Models\UserService;
use App\Models\UserServiceSchedule;
use App\Models\ServiceTransaction;

class BookingContoller extends Controller
{
  public function index($id){
    $cities = City::get()->all();
    $activities = Activity::get()->all();
    $times = Time::get()->all();

    $heroBanner = $this->getHeroBanner();
    $callToAction = $this->getCallToAction();
    $socialMedia = $this->getSocailMedia();

    $service = $this->getServiceDetail($id);

    return view('/content/member/booking/index', ['id'=>$id, 'heroBanner'=>$heroBanner, 'callToAction'=>$callToAction, 'socialMedia'=>$socialMedia, 'cities'=>$cities, 'times'=>$times, 'service'=>$service]);
  }

  public function getServiceDetail($id){

    $service = UserService::where('id', $id)->get()->first();

    $user = User::where('id', $service->user_id)->get()->first();
    $price = Price::where('id', $service->price)->get()->first();
    $activity = Activity::where('id', $service->activity_id)->get()->first();

    $datas = [];
    $datas['id'] = $service->id;
    $datas['user_id'] = $user->user_id;
    $datas['user_name'] = $user->name;
    $datas['user_image'] = $user->image_profile;
    $datas['participant'] = $service->participant;
    $datas['activity_name'] = $activity->name;
    $datas['price'] = $price->name;

    return $datas;

  }

  public function getHeroBanner(){

    $data = [];
    
    $heroBanner = HeroBanner::where('id', 2)->get()->first();
    $data['image'] = $heroBanner->image;

    if (App::isLocale('id')) {
      $data['title'] = $heroBanner->title;
    } else{
      $data['title'] = $heroBanner->title_en;
    }

    return $data;
  }
  
  public function getCallToAction(){
  
    $datas = [];

    $callToAction = CallToAction::get()->last();

    $datas['image'] = $callToAction->image;
    $datas['link'] = $callToAction->link;

    if (App::isLocale('id')) {
      $datas['title'] = $callToAction->title;
    } else{
      $datas['title'] = $callToAction->title_en;
    }

    return $datas;
  }
    
  public function getSocailMedia(){
    $socialMedia = SocialMedia::get()->all();
    return $socialMedia;
  }

  public function store(Request $request){

    if (empty($request->location)){
        return response()->json(['success' => false, 'message' => "Destination cannot be empty"], 400);
    }

    if (empty($request->start_time)){
        return response()->json(['success' => false, 'message' => "Start time cannot be empty"], 400);
    }

    if (empty($request->end_time)){
        return response()->json(['success' => false, 'message' => "End time cannot be empty"], 400);
    }

    if (empty($request->date)){
        return response()->json(['success' => false, 'message' => "Date cannot be empty"], 400);
    }

    $startTime = Carbon::createFromFormat('H:i', $request->start_time);
    $endTime = Carbon::createFromFormat('H:i', $request->end_time);

    if ($startTime->gt($endTime)){
        return response()->json(['success' => false, 'message' => "start time cannot grater than end time"], 400);
    }

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    $lastTransaction = ServiceTransaction::get()->last();

    $serviceTransaction = new ServiceTransaction;
    $serviceTransaction->user_id = $userId;
    $serviceTransaction->service_id = $request->service_id;

    $now = Carbon::now()->format('Ymd');

    $invoiceNo = '';

    if (!empty($lastTransaction)){
        $invoiceNo = 'INV/' . $now . '/' . $userId . $request->service_id . '/00' . $lastTransaction->id + 1;
    } else {
        $invoiceNo = 'INV/' . $now . '/' . $userId . $request->service_id . '/00' . 1;
    }

    $serviceTransaction->invoice_no = $invoiceNo;
    $serviceTransaction->location = $request->location;
    $serviceTransaction->location_lat = $request->location_lat;
    $serviceTransaction->location_long = $request->location_long;
    $serviceTransaction->participant = $request->participant;
    $serviceTransaction->start_time = $startTime->format('H:i:s');
    $serviceTransaction->end_time = $endTime->format('H:i:s');
    $serviceTransaction->duration = $request->duration;
    $serviceTransaction->date = Carbon::createFromFormat('d-m-Y', $request->date)->format('Y-m-d');

    $service = UserService::where('id', $request->service_id)->get()->first();
    $price = Price::where('id', $service->price)->get()->first();

    $totalPrice = ($request->participant * $request->duration) * $price->name;
    $feePrice = $totalPrice / 10;

    $serviceTransaction->total_price = $totalPrice;
    $serviceTransaction->service_price = $totalPrice - $feePrice;
    $serviceTransaction->fee_price = $feePrice;
    $serviceTransaction->booked_at = Carbon::now();
    $serviceTransaction->status = 1;
    $serviceTransaction->buddy_id = $service->user_id;

    $serviceTransaction->save();

    return response()->json(['success' => true, 'message' => "booking submitted"], 201);

  }

  public function checkout(){
  
    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    $heroBanner = $this->getHeroBanner();
    $callToAction = $this->getCallToAction();
    $socialMedia = $this->getSocailMedia();

    $bookedService = ServiceTransaction::where('user_id', $userId)->where('status', 1)->get()->last();

    $service = UserService::where('id', $bookedService->service_id)->get()->first();
    $price = Price::where('id', $service->price)->get()->first();

    $paymentMethod = array();
    $payments = Payment::get()->all();

    foreach ($payments as $data) {

      $payment = [];

      if ($data->type == 1){

        $banks = Bank::where('id', $data->bank_id)->get()->first();

        $payment['id'] = $data->id;
        $payment['name'] = $banks->name;

      } else {
        $payment['id'] = $data->id;
        $payment['name'] = 'Saldo';
      }
      

      array_push($paymentMethod, $payment);
    }

    return view('/content/member/checkout/index', ['heroBanner'=>$heroBanner, 'callToAction'=>$callToAction, 'socialMedia'=>$socialMedia, 'service'=>$bookedService, 'price'=>$price, 'paymentMethod'=>$paymentMethod]);
  }

  public function checkoutStore(Request $request, $id){

    $service = ServiceTransaction::where('id', $id)->get()->first();

    $service->payment_type = $request->payment;
    $service->status = 2;
    $service->save();

    return response()->json(['success' => true, 'message' => "checkout success"], 200);

  }

  public function paymentDetail($id){

    $payment = Payment::where('id', $id)->get()->first();

    $data = [];

    if ($payment->type == 1){

      $banks = Bank::where('id', $payment->bank_id)->get()->first();

      $data['name'] = $banks->name;
      $data['image'] = $banks->icon;
      $data['bank_account'] = $payment->name;
      $data['description'] = $payment->description;

    } else {
      $data['name'] = 'Saldo';
      $data['image'] = 'somebuddy-logo.png';
      $data['bank_account'] = $payment->name;
      $data['description'] = $payment->description;
    }
    
    return response()->json(['success' => true, 'message' => "success", 'data'=>$data], 200);
  }
}
