<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserService;
use App\Models\ServiceTransaction;
use App\Models\Master\Activity;

class TransactionController extends Controller
{
  public function index(Request $request){

    if ($request->has('status')){
      $transactions = $this->getTransactions($request);
      $status = $request->status;
    } else {
      $transactions = $this->getTransactions('', '', 'new');
      $status = 'new';
    }

    return view('/content/member/transaction/index', ['transactions'=>$transactions, 'status'=>$status]);
  }

  public function getTransactions($request){

    $user = Auth::user();
    $userId = Auth::user()->id;
    $datas = array();

    $qs = '';

    if (!empty($request->search)){
      $qs = $request->search;
    }

    $startDate = Carbon::now()->subYear()->toDateString();
    $endDate = Carbon::now()->addYear()->toDateString();

    if (!empty($request->date)){
      $dates = explode(" to ", strval($request->date));
      $startDate = $dates[0];
      $endDate = $dates[1];
    }

    if(Auth::user()->hasRole('member')){
      $filterStatus = [1,2,3];
    } else {
      $filterStatus = [3];
    }

    if (!empty($request->status)){
      switch($request->status) {

        case('all'): 
          if(Auth::user()->hasRole('member')){
            $filterStatus = [1,2,3,4,5,6];
          } else {
            $filterStatus = [3,4,5,6];
          }
          break;
  
        case('new'): 
          if(Auth::user()->hasRole('member')){
            $filterStatus = [1,2,3];
          } else {
            $filterStatus = [3];
          }
          break;
  
        case('ongoing'): 
          $filterStatus = [4];
          break;
  
        case('done'): 
          $filterStatus = [5];
          break;
  
        case('complain'): 
          $filterStatus = [6];
          break;
  
      }
    }

    if(Auth::user()->hasRole('member')){

      $query = ServiceTransaction::where('service_transaction.user_id', $userId)
      ->whereIn('service_transaction.status', $filterStatus)
      ->where('t2.name', 'like', '%' . $qs . '%')
      ->whereDate('date', '<=', $endDate)
      ->whereDate('date', '>=', $startDate)
      ->join('user_service as t1', 't1.id', '=', 'service_transaction.service_id')
      ->join('users as t2', 't2.id', '=', 'service_transaction.buddy_id')
      ->join(config('database.connections.mysql_master.database') . '.activities as t3', 't3.id', '=', 't1.activity_id')
      ->get(['service_transaction.id','t2.name as user_name','t2.image_profile as user_image','t3.name as service_name','service_transaction.invoice_no','service_transaction.duration','service_transaction.participant','service_transaction.total_price','service_transaction.start_time','service_transaction.end_time','service_transaction.date','service_transaction.status']);

    }  else {

      $query = ServiceTransaction::where('service_transaction.buddy_id', $userId)
        ->whereIn('service_transaction.status', $filterStatus)
        ->where('t2.name', 'like', '%' . $qs . '%')
        ->whereDate('date', '<=', $endDate)
        ->whereDate('date', '>=', $startDate)
        ->join('user_service as t1', 't1.id', '=', 'service_transaction.service_id')
        ->join('users as t2', 't2.id', '=', 'service_transaction.user_id')
        ->join(config('database.connections.mysql_master.database') . '.activities as t3', 't3.id', '=', 't1.activity_id')
        ->get(['service_transaction.id','t2.name as user_name','t2.image_profile as user_image','t3.name as service_name','service_transaction.invoice_no','service_transaction.duration','service_transaction.participant','service_transaction.total_price','service_transaction.start_time','service_transaction.end_time','service_transaction.date','service_transaction.status']);
    
      }
    
    foreach ($query as $item){

      $data = [];
      $data['id'] = $item->id;
      $data['name'] = $item->user_name;
      $data['image'] = $item->user_image;
      $data['service_name'] = $item->service_name;
      $data['invoice_no'] = $item->invoice_no;
      $data['duration'] = $item->duration;
      $data['participant'] = $item->participant;
      $data['total_price'] = $item->total_price;
      $data['start_time'] = Carbon::createFromFormat('H:i:s', $item->start_time)->format('H:i');
      $data['end_time'] = Carbon::createFromFormat('H:i:s', $item->end_time)->format('H:i');
      $data['date'] = Carbon::parse($item->date)->format('d M Y');
      $data['status'] = $item->status;
  
      array_push($datas, $data);
    }

    return $datas;
  }

  public function getTransaction($query, $date, $status){

    $userId = Auth::user()->id;
    $datas = array();

    if ($status == 'all') {
      $serviceTransactions = ServiceTransaction::where('user_id', $userId)->orWhere('buddy_id', $userId)->get()->all();
    }

    if ($status == 'new') {
      $serviceTransactions = ServiceTransaction::where('user_id', $userId)->orWhere('buddy_id', $userId)->whereIn('status', [1,2,3])->get()->all();
    }

    if ($status == 'ongoing') {
      $serviceTransactions = ServiceTransaction::where('user_id', $userId)->orWhere('buddy_id', $userId)->where('status', 4)->get()->all();
    }

    if ($status == 'done') {
      $serviceTransactions = ServiceTransaction::where('user_id', $userId)->orWhere('buddy_id', $userId)->where('status', 5)->get()->all();
    }

    if ($status == 'complain') {
      $serviceTransactions = ServiceTransaction::where('user_id', $userId)->orWhere('buddy_id', $userId)->where('status', 6)->get()->all();
    }

    foreach($serviceTransactions as $data){

      if ($data->user_id != $userId){
        
        if($data->status != 2 && $data->status != 1){
          
          $service = UserService::where('id', $data->service_id)->get()->first();
          $activity = Activity::where('id', $service->activity_id)->get()->first();
          $user = User::where('id', $data->user_id)->get()->first();

          $transaction = [];
          $transaction['name'] = $user->name;
          $transaction['image'] = $user->image_profile;
          $transaction['service_name'] = $activity->name;
          $transaction['invoice_no'] = $data->invoice_no;
          $transaction['duration'] = $data->duration;
          $transaction['participant'] = $data->participant;
          $transaction['total_price'] = $data->total_price;
          $transaction['start_time'] = Carbon::createFromFormat('H:i:s', $data->start_time)->format('H:i');
          $transaction['end_time'] = Carbon::createFromFormat('H:i:s', $data->end_time)->format('H:i');
          $transaction['date'] = Carbon::parse($data->date)->format('d M Y');
          $transaction['status'] = $data->status;
  
          array_push($datas, $transaction);
        }

      } else{
        $service = UserService::where('id', $data->service_id)->get()->first();
        $activity = Activity::where('id', $service->activity_id)->get()->first();
        $user = User::where('id', $service->user_id)->get()->first();

        $transaction = [];
        $transaction['name'] = $user->name;
        $transaction['image'] = $user->image_profile;
        $transaction['service_name'] = $activity->name;
        $transaction['invoice_no'] = $data->invoice_no;
        $transaction['duration'] = $data->duration;
        $transaction['participant'] = $data->participant;
        $transaction['total_price'] = $data->total_price;
        $transaction['start_time'] = Carbon::createFromFormat('H:i:s', $data->start_time)->format('H:i');
        $transaction['end_time'] = Carbon::createFromFormat('H:i:s', $data->end_time)->format('H:i');
        $transaction['date'] = Carbon::parse($data->date)->format('d M Y');
        $transaction['status'] = $data->status;

        array_push($datas, $transaction);
      }

    
    }

    return $datas;
  }

}
