<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
  public function index(){
    return view('/content/member/dashboard/index');
  }

  public function indexBuddy(){
    return view('/content/buddy/dashboard/index');
  }

}
