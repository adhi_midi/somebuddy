<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UserReview;
use App\Models\UserService;
use App\Models\Master\Activity;

class ReviewController extends Controller
{
  public function index(){

    $reviews = $this->getReviews();

    return view('/content/member/review/index', ['reviews'=>$reviews]);
  }

  public function getReviews(){

    $userId = Auth::user()->id;

    $datas = array();

    $userReviews = UserReview::where('user_id', $userId)->orWhere('buddy_id', $userId)->get()->all();

    foreach($userReviews as $data){

      if ($userId == $data->user_id){
        $service = UserService::where('id', $data->service_id)->get()->first();
        $activity = Activity::where('id', $service->activity_id)->get()->first();
        $user = User::where('id', $service->user_id)->get()->first();
      } else {
        $service = UserService::where('id', $data->service_id)->get()->first();
        $activity = Activity::where('id', $service->activity_id)->get()->first();
        $user = User::where('id', $data->user_id)->get()->first();
      }
        
      $review = [];
      $review['rating'] = $data->rating;
      $review['desc'] = $data->description;
      $review['name'] = $user->name;
      $review['image'] = $user->image_profile;
      $review['service'] = $activity->name;

      array_push($datas, $review);
    }

    return $datas;
  }

}
