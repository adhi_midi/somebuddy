<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResolutionController extends Controller
{
  public function index(){
    return view('/content/member/resolution/index');
  }

  public function indexBuddy(){
    return view('/content/buddy/resolution/index');
  }
}
