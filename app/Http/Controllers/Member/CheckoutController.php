<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UserService;
use App\Models\UserServiceSchedule;
use App\Models\ServiceTransaction;

class CheckoutController extends Controller
{
    public function index(){
        
        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);


        return view('/content/member/checkout/index');
    }

    public function detail(Request $request){

        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);


        return view('/content/member/checkout/index');
    }


    public function store(Request $request){
        return response()->json(['success' => true, 'message' => "checkout submitted"], 200);
    }
}
