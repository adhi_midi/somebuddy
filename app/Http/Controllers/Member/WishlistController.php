<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\City;
use App\Models\Master\Activity;
use App\Models\Master\Price;
use App\Models\User;
use App\Models\UserService;
use App\Models\UserWishlist;

class WishlistController extends Controller
{
  public function index(){

    $wishlists = $this->getWishlists();

    return view('/content/member/wishlist/index', ['wishlists'=>$wishlists]);
  }

  public function getWishlists(){

    $user = Auth::user();

    $datas = array();

    $query = UserWishlist::where('user_wishlist.user_id', $user->id)->where('user_wishlist.status', 1)
        ->join('user_service as t1', 't1.id', '=', 'user_wishlist.service_id')
        ->join('users as t2', 't2.id', '=', 't1.user_id')
        ->join(config('database.connections.mysql_master.database') . '.cities as t3', 't3.city_id', '=', 't2.location')
        ->join(config('database.connections.mysql_master.database') . '.activities as t4', 't4.id', '=', 't1.activity_id')
        ->join(config('database.connections.mysql_master.database') . '.prices as t5', 't5.id', '=', 't1.price')
        ->get(['t1.id', 't1.rating','t2.id as user_id','t2.name as user_name', 't2.image_profile', 't3.city_name', 't4.name as service_name', 't5.name as price']);

    foreach ($query as $item){ 

      $data = [];
      $data['user_id'] = $item->user_id;
      $data['user_name'] = $item->user_name;
      $data['user_image'] = $item->image_profile;
      $data['service_id'] = $item->id;
      $data['service_name'] = $item->service_name;
      $data['price'] = $item->price;
      $data['location'] = $item->city_name;
      $data['rating'] = $item->rating;
      $data['wishlist'] = true;

      array_push($datas, $data);
    }

    return $datas;

  }

  public function store(Request $request){

    $user = Auth::user();

    $wishlist = UserWishlist::where('user_id', $user->id)->where('service_id', $request->id)->get()->first();

    if (empty($wishlist)){

      $newWishlist = new UserWishlist;

      $newWishlist->user_id = $user->id;
      $newWishlist->service_id = $request->id;
      $newWishlist->status = 1;
      $newWishlist->save();

      return response()->json(['success' => true, 'message' => 'Success added to Wishlist'], 200);

    } 

    if ($wishlist->status == 0){

      $wishlist->status = 1;
      $wishlist->save();

      return response()->json(['success' => true, 'message' => 'Success added to Wishlist'], 200);
    
    }
    
    $wishlist->status = 0;
    $wishlist->save();

    return response()->json(['success' => true, 'message' => 'Success removed from Wishlist'], 200);

  }

}
