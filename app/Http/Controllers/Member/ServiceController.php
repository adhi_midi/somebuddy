<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserService;
use App\Models\UserServiceSchedule;
use App\Models\Master\Price;
use App\Models\Master\Time;
use App\Models\Master\Activity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ServiceController extends Controller
{
  public function index(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);
    $userService = UserService::where('user_id', $userId)->get()->all();
    $prices = Price::get()->all();
    $activities = Activity::get()->all();

    return view('/content/member/settings/services/index',['user'=>$user, 'userService'=>$userService, 'prices'=>$prices, 'activities'=>$activities]);
  }

  public function create(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    $this->deteleDraft($userId);
    
    $draftService = new UserService;
    $draftService->user_id = $userId;
    $draftService->status = 0;
    $draftService->save();

    $prices = Price::get()->all();
    $times = Time::get()->all();
    $activities = Activity::get()->all();

    $currentService = UserService::where('user_id', $userId)->get()->last();
    $currentServiceSchedule = UserService::where('user_id', $userId)->get()->last();

    $existActivities = array();
    $existServices = UserService::where('user_id', $userId)->whereNotNull('activity_id')->get()->all();
    foreach ($existServices as $data){
      array_push($existActivities, $data->activity_id);
    }

    return view('/content/member/settings/services/add',['user'=>$user, 'service'=>$currentService, 'serviceSchedule'=>$currentServiceSchedule, 'existActivities'=>$existActivities, 'prices'=>$prices, 'times'=>$times, 'activities'=>$activities]);
  }

  public function detail($id){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);
    $service = UserService::where('id', $id)->get()->first();
    $serviceSchedules = UserServiceSchedule::where('service_id', $id)->where('status', 1)->get()->all();
    $prices = Price::get()->all();
    $times = Time::get()->all();
    $activities = Activity::get()->all();

    return view('/content/member/settings/services/detail',['user'=>$user, 'service'=>$service, 'serviceSchedules'=>$serviceSchedules, 'prices'=>$prices, 'times'=>$times,'activities'=>$activities]);
  }

  public function store(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    $service = UserService::where('user_id', $userId)->get()->last();

    // $validator = Validator::make($request->all(), [
    //   'activity' => ['required', 'integer'],
    //   'price' => ['required', 'integer'],
    //   'participant' => ['required', 'integer'],
    //   'schedules' => ['required', 'string'],
    // ]);

    if (empty($request->participant)){
      return response()->json(['error' => true, 'message' => 'Participant cannot be empty'], 400);
    }

    if (empty($request->price)){
      return response()->json(['error' => true, 'message' => 'Price cannot be empty'], 400);
    }

    if (empty($request->activity)){
      return response()->json(['error' => true, 'message' => 'Activity cannot be empty'], 400);
    }

    if (empty($request->schedules)){
      return response()->json(['error' => true, 'message' => 'Schedules cannot be empty'], 400);
    }

    $service->user_id = $userId;
    $service->activity_id = intval($request->activity);
    $service->price = intval($request->price);
    $service->participant = intval($request->participant);
    $service->status = 1;

    $service->save();

    $serviceSchedules = $request->schedules;

    foreach ($serviceSchedules as $schedule){
      $serviceSchedule = new UserServiceSchedule;
      $serviceSchedule->user_id = $userId;
      $serviceSchedule->service_id = $service->id;
      $serviceSchedule->days = json_encode($schedule['days']);
      $serviceSchedule->start_time = Carbon::createFromFormat('H:i', $schedule['start_time'])->format('H:i:s');
      $serviceSchedule->end_time = Carbon::createFromFormat('H:i', $schedule['end_time'])->format('H:i:s');
      $serviceSchedule->status = 1;
      $serviceSchedule->save();
    }

    return response()->json(['success' => true, 'message' => 'New Data Created'], 200);
    
  }

  public function update(Request $request, $id){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    $service = UserService::where('id', $id)->get()->last();

    $service->activity_id = intval($request->activity);
    $service->price = intval($request->price);
    $service->participant = intval($request->participant);

    $service->save();

    $serviceSchedules = $request->schedules;

    if (empty($request->participant)){
      return response()->json(['error' => true, 'message' => 'Participant cannot be empty'], 400);
    }

    if (empty($request->price)){
      return response()->json(['error' => true, 'message' => 'Price cannot be empty'], 400);
    }

    if (empty($request->activity)){
      return response()->json(['error' => true, 'message' => 'Activity cannot be empty'], 400);
    }

    if (empty($request->schedules)){
      return response()->json(['error' => true, 'message' => 'Schedules cannot be empty'], 400);
    }

    foreach ($serviceSchedules as $schedule){
      if ($schedule['id'] != 0){
        $serviceSchedule = UserServiceSchedule::where('id', $schedule['id'])->get()->first();
        $serviceSchedule->service_id = $service->id;
        $serviceSchedule->days = json_encode($schedule['days']);
        $serviceSchedule->start_time = Carbon::createFromFormat('H:i', $schedule['start_time'])->format('H:i:s');
        $serviceSchedule->end_time = Carbon::createFromFormat('H:i', $schedule['end_time'])->format('H:i:s');
        $serviceSchedule->save();

      } else {
        $serviceSchedule = new UserServiceSchedule;
        $serviceSchedule->user_id = $userId;
        $serviceSchedule->service_id = $service->id;
        $serviceSchedule->days = json_encode($schedule['days']);
        $serviceSchedule->start_time = Carbon::createFromFormat('H:i', $schedule['start_time'])->format('H:i:s');
        $serviceSchedule->end_time = Carbon::createFromFormat('H:i', $schedule['end_time'])->format('H:i:s');
        $serviceSchedule->status = 1;
        $serviceSchedule->save();
      }
    }

    return response()->json(['success' => true, 'message' => 'New Data Created'], 200);
  }

  public function delete($id){
    $service = UserService::where('id', $id)->delete();
    $schedule = UserServiceSchedule::where('service_id', $id)->delete();
    return response()->json(['success' => true, 'message' => 'Data deleted'], 200);
  }

  public function deleteServiceSchedule($id){
    $schedule = UserServiceSchedule::where('id', $id)->delete();
    return response()->json(['success' => true, 'message' => 'Schedule deleted'], 200);
  }

  public function deteleDraft($id){
    $draftService = UserService::where('user_id', $id)->where('status', 0);
    $draftService->delete();

    $draftServiceSchedule = UserServiceSchedule::where('user_id', $id)->where('status', 0);
    $draftServiceSchedule->delete();
  }

}
