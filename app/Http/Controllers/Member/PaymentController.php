<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;

class PaymentController extends Controller
{
  public function index(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    return view('/content/member/settings/payment/index',['user'=>$user]);
  }
    
  public function store(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    return back()->with('success',"store successfully"); 
  }
    
  public function update(Request $request){

    $userId = Auth::user()->id;
    $user = User::findOrFail($userId);

    return back()->with('success',"update successfully"); 
  }
}
