<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserBalance;
use App\Models\UserBalanceLog;

class WalletController extends Controller
{
  public function index(Request $request){

    $user = Auth::user();
    $balance = $this->getUserBalance($user->id);
    $logBalance = $this->getUserLogBalance($user->id);

    return view('/content/member/wallet/index', ['balance'=>$balance, 'logBalance'=>$logBalance]);
  }

  public function getUserBalance($id){

    $userBalance = UserBalance::where('user_id', $id)->get()->first();

    $data = number_format($userBalance->balance, 0, ',', '.');

    return $data;
  }

  public function getUserLogBalance($id){

    $datas = array();

    return $datas;
  }

  public function withdraw(Request $request){

    $user = Auth::user();
    $balance = $this->getUserBalance($user->id);

    return view('/content/member/wallet/withdraw', ['balance'=>$balance]);
  }

  public function deposit(){

    $user = Auth::user();

    return view('/content/member/wallet/deposit');
  }

  public function bank(){

    $user = Auth::user();
    $balance = $this->getUserBalance($user->id);

    return view('/content/member/wallet/bank');
  }

  public function withdrawStore(Request $request){
    
    return response()->json(['success' => true, 'message' => 'Withdraw request submitted'], 200);
  }

  public function depositStore(Request $request){
    
    return response()->json(['success' => true, 'message' => 'Deposit request submitted'], 200);
  }

  public function bankStore(Request $request){
    
    return response()->json(['success' => true, 'message' => 'New bank account submitted'], 200);
  }
  
}
