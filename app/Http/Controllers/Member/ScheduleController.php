<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
  public function index(){
    return view('/content/member/schedule/index');
  }

  public function indexBuddy(){
    return view('/content/buddy/schedule/index');
  }

}
