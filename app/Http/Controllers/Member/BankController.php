<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class BankController extends Controller
{
    public function index(Request $request){

        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);

        return view('/content/member/settings/bank/index',['user'=>$user]);
    }

    public function store(Request $request){

        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);

        return back()->with('success',"store successfully"); 
    }

    public function update(Request $request){

        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);

        return back()->with('success',"update successfully"); 
    }
}
