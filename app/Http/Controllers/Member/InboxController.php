<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InboxController extends Controller
{
  public function index(){
    return view('/content/member/inbox/index');
  }

  public function indexBuddy(){
    return view('/content/buddy/inbox/index');
  }
}
