<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('service_id');
            $table->integer('user_id');
            $table->string('invoice_no');
            $table->string('location');
            $table->integer('participant');
            $table->datetime('start_time');
            $table->datetime('end_time');
            $table->integer('duration');
            $table->datetime('date');
            $table->integer('total_price');
            $table->integer('service_price');
            $table->integer('fee_price');
            $table->integer('payment_type');
            $table->integer('status_type');
            $table->datetime('booked_at')->nullable();
            $table->datetime('confirm_at')->nullable();
            $table->datetime('confirm_payment_at')->nullable();
            $table->datetime('finish_at')->nullable();
            $table->boolean('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_transaction');
    }
}
