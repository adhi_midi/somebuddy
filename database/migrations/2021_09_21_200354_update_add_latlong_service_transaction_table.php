<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddLatlongServiceTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_transaction', function (Blueprint $table) {
            $table->float('location_lat')->nullable()->default(0);
            $table->float('location_long')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_transaction', function (Blueprint $table) {
            $table->dropColumn('location_lat');
            $table->dropColumn('location_long');
        });
    }
}
