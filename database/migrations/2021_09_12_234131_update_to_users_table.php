<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone_number')->unique()->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->integer('gender')->nullable()->change();
            $table->string('image_profile')->nullable()->change();
            $table->integer('location')->nullable()->change();
            $table->integer('age')->nullable()->change();
            $table->integer('user_type')->nullable()->change();
            $table->integer('activity')->nullable()->change();
            $table->integer('interest')->nullable()->change();
            $table->string('about')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone_number');
            $table->dropColumn('address');
            $table->dropColumn('gender');
            $table->dropColumn('image_profile');
            $table->dropColumn('location');
            $table->dropColumn('age');
            $table->dropColumn('user_type');
            $table->dropColumn('activity');
            $table->dropColumn('interest');
            $table->dropColumn('about');
        });
    }
}
