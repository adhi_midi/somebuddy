<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_master')->create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bank_id')->unsigned()->nullable()->default(0);
            $table->string('name')->nullable();
            $table->integer('type')->unsigned()->nullable()->default(1);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_master')->dropIfExists('payments');
    }
}
