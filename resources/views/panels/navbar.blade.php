@if($configData["showMenu"] === false && isset($configData["showMenu"]))
<nav class="header-navbar navbar-expand-lg navbar navbar-sticky align-items-center navbar-shadow navbar-brand-center navbar-home {{ $configData['navbarColor'] }}" data-nav="brand-center">
  <div class="navbar-header d-xl-block d-none">
    <ul class="nav navbar-nav">
      <li class="nav-item">
        <a class="navbar-brand nav-dark ml-1" href="{{url('/')}}">
          <span class="brand-logo d-inline-block">
            <img src="{{asset('images/logo/somebuddy-logo-light.png')}}" height="40">
          </span>
        </a>
      </li>
    </ul>
  </div>
  @else
  <nav class="header-navbar navbar navbar-expand-lg align-items-center {{ $configData['navbarClass'] }} navbar-light navbar-shadow {{ $configData['navbarColor'] }}">
    @endif
    <div class="navbar-container d-flex content">
      <div class="bookmark-wrapper d-flex align-items-center">
        <ul class="nav navbar-nav d-xl-none">
          <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i class="ficon" data-feather="menu"></i></a></li>
        </ul>
        <ul class="nav navbar-nav">
          <li class="nav-item dropdown dropdown-language">
            <a class="nav-link dropdown-toggle" id="dropdown-flag" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="flag-icon flag-icon-us"></i>
              <span class="selected-language">EN</span>
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdown-flag">
              <a class="dropdown-item" href="{{url('lang/en')}}" data-language="en">
                <i class="flag-icon flag-icon-us"></i> EN
              </a>
              <a class="dropdown-item" href="{{url('lang/id')}}" data-language="id">
                <i class="flag-icon flag-icon-id"></i> ID
              </a>
            </div>
          </li>
        </ul>
      </div>
      <ul class="nav navbar-nav align-items-center ml-auto">
        <li class="nav-item dropdown dropdown-user">
          <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="user-nav d-sm-flex d-none">
              <span class="user-name font-weight-bolder">{{ Auth::user()->name }}</span>
            </div>
            <span class="avatar">
              @if(!empty(Auth::user()->image_profile))
                <img class="round" src="{{asset('images/profile/thumb/' . Auth::user()->image_profile )}}" alt="avatar" height="40" width="40">
              @else
                <img class="round" src="{{asset('images/profile/thumb/default_avatar.png')}}" alt="avatar" height="40" width="40">
              @endif
            </span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
            <a class="dropdown-item" href="{{ route('signout') }}" data-i18n="key">
              <i class="mr-50" data-feather="power"></i> {{ __('locale.logout') }}
            </a>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  <!-- END: Header-->
