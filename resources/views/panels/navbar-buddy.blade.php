
<nav class="header-navbar navbar-expand-lg navbar navbar-sticky align-items-center navbar-shadow navbar-brand-center navbar-home {{ $configData['navbarColor'] }}" data-nav="brand-center">
  <div class="navbar-header">
    <ul class="nav navbar-nav">
      <li class="nav-item">
        <a class="navbar-brand nav-dark ml-1" href="{{url('/')}}">
          <span class="brand-logo d-inline-block">
            <img src="{{asset('images/logo/somebuddy-logo-dark.png')}}" height="40">
          </span>
        </a>
      </li>
    </ul>
  </div>
  <div class="navbar-container d-flex content">
    <ul class="nav navbar-nav ml-auto mr-2">
      <li class="nav-item dropdown dropdown-language">
        <a class="nav-link dropdown-toggle mt-25" id="dropdown-flag" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="flag-icon flag-icon-us"></i>
          <span class="selected-language">EN</span>
        </a>
        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdown-flag">
          <a class="dropdown-item" href="{{url('lang/en')}}" data-language="en">
            <i class="flag-icon flag-icon-us"></i> EN
          </a>
          <a class="dropdown-item" href="{{url('lang/id')}}" data-language="id">
            <i class="flag-icon flag-icon-id"></i> ID
          </a>
        </div>
      </li>
    </ul>
    @guest
      <ul class="nav navbar-nav align-items-center">
        <li>
          <a class="text-white" href="{{ route('login') }}" >
              <i class="mr-50" data-feather="user"></i> Login
            </a>
        </li>
      </ul>
    @endguest
    @auth
    <ul class="nav navbar-nav align-items-center">
      <li class="nav-item dropdown dropdown-user">
        <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="user-nav d-sm-flex d-none">
            <span class="user-name font-weight-bolder m-0">{{ Auth::user()->name }}</span>
          </div>
          <span class="avatar">
            @if(!empty(Auth::user()->image_profile))
              <img class="round" src="{{asset('images/profile/thumb/' . Auth::user()->image_profile )}}" alt="avatar" height="40" width="40">
            @else
              <img class="round" src="{{asset('images/profile/thumb/default_avatar.png')}}" alt="avatar" height="40" width="40">
            @endif
          </span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
          <a class="dropdown-item" href="{{ route('dashboard') }}">
            <i class="mr-50" data-feather="user"></i> {{ __('locale.member_area') }}
          </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('signout') }}">
            <i class="mr-50" data-feather="power"></i> {{ __('locale.logout') }}
          </a>
        </div>
      </li>
    </ul>
    @endauth
  </div>
</nav>
  <!-- END: Header-->
