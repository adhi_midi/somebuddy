<!-- BEGIN: Footer-->
<footer class="footer footer-dark">
  <p class="clearfix mb-0 text-center">
    <span class="d-block d-md-inline-block mt-25">copyright &copy; 2021<a class="ml-25" href="/" >SOMEBUDDY</a>
      <span class="d-none d-sm-inline-block">, All rights Reserved</span>
    </span>
  </p>
</footer>
<!-- END: Footer-->
