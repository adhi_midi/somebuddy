@extends('layouts/indexBuddyLayoutMaster')

@section('title', 'Buddies')

@section('content')
<!-- Page layout -->
<section class="checkout-hero" style="background-image: url('{{asset('images/banners/' . $heroBanner['image'])}}');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="font-large-2 text-white title">{{ __('locale.checkout') }}</h3>
            </div>
        </div>
    </div>
</section>

<section class="checkout-detail mb-5" data-checkout="{{$service->id}}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="card card-checkout" style="min-height:500px">
                    <div class="card-body">
                        <h3 class="font-weight-bolder font-large-1 text-primary mt-1 mb-3">
                            {{ __('locale.order_detail') }}
                        </h3>

                        <div class="row mb-3 m-0">
                            <div class="col-md-3">
                                <p class="mb-25">{{ __('locale.participant') }}</p>
                                <p class="font-medium-3 font-weight-bolder text-primary sum-participant">
                                    <span class="total-participant">{{$service->participant}}</span> x Rp. {{ number_format($price->name, 0, ',', '.') }}
                                </p>
                            </div>
                            <div class="col-md-3">
                                <p class="mb-25">{{ __('locale.duration') }}</p>
                                <p class="font-medium-3 font-weight-bolder text-primary">
                                    <span class="total-hour">{{$service->duration}}</span> x Rp. {{ number_format($price->name, 0, ',', '.') }}
                                </p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p class="mb-25">Sub Total</p>
                                <p class="font-medium-5 font-weight-bolder text-primary">
                                    Rp. <span class="total-price">{{ number_format($service->total_price, 0, ',', '.') }}</span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        <div class="row mb-3 m-0 pt-3">
                            <div class="col-md-6">
                                <label class="form-label" for="payment">{{ __('locale.payment_method') }}</label>
                                <select name="payment" class="select2 form-control select-payment" data-minimum-results-for-search="Infinity" required>
                                    @foreach($paymentMethod as $key => $data)  
                                        <option value="{{ $data['id'] }}">{{ $data['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 payment-detail text-center">
                                <div class="image-bank mb-2">
                                    <img class="image" height="70">
                                </div>
                                <p class="font-large-1 font-weight-bolder text-primary payment-title mt-1 mb-1"></p>
                                <p class="font-medium-2 text-primary payment-desc"></p>
                            </div>
                        </div>
                        <div class="row mb-2 m-0">
                            <div class="col-md-12">
                                <button class="btn btn-success w-100 btn-checkout">{{ __('locale.checkout') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-call-to-action mb-5 py-5" style="background-image: url('{{asset('images/banners/' . $callToAction['image'])}}');">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <div class="title mb-3" data-sal="fade" data-sal-duration="1000" data-sal-delay="">
                    <h2 class="font-large-2 font-weight-bolder text-center text-white">
                        {{ $callToAction['title'] }}
                    </h2>
                </div>
                <a href="{{ $callToAction['link'] }}" class="btn btn-light" data-sal="fade" data-sal-duration="1000" data-sal-delay="200">{{ __('landing.learn_more') }}</a>
            </div>
        </div>
    </div>
</section>

<section class="home-connect mb-5" >
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <div class="title mb-2" data-sal="slide-down" data-sal-duration="1000">
                    <h2 class="font-large-1 font-weight-bolder text-center">
                        {{ __('landing.connect_with_us') }}
                    </h2>
                </div>
                <ul class="nav-socmed p-0">
                    @foreach($socialMedia as $key => $data)
                        @if ($data->name == 'facebook')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000">
                                    <i data-feather="facebook"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'instagram')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="200">
                                    <i data-feather="instagram"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'twitter')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400">
                                    <i data-feather="twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'youtube')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="600">
                                    <i data-feather="youtube"></i>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>

<!--/ Page layout -->
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/home/checkout.js')) }}"></script>
@endsection
