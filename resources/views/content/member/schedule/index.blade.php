@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Schedule')
@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/calendars/fullcalendar.min.css')) }}">
@endsection
@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(('css/base/pages/app-calendar.css')) }}">
  <link rel="stylesheet" href="{{ asset(('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
<section>
  <div class="data-type" data-user-type="1"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.schedule') }}</h2>
</section>

<section>
    <div class="app-calendar">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body text-center">
                        <button type="button" class="btn btn-icon btn-flat-primary ">
                            <i data-feather="chevron-left"></i>
                        </button>
                        <span class="font-medium-2 font-weight-bolder text-primary ">April 22 - 26, 2021</span>
                        <button type="button" class="btn btn-icon btn-flat-primary ">
                            <i data-feather="chevron-right" height="50" width="50"></i>
                        </button>
                    </div>
                    <div class="card-footer">
                        <div class="session-list">
                            <!-- <div class="session-item">
                                <div class="divider divider-left">
                                    <div class="divider-text font-weight-bolder">Thursday, 22 April 2021 </div>
                                </div>
                                <div class="card">
                                    <div class="card-body border-left-primary">
                                        <div class="row g-0">
                                            <div class="col-2">
                                                <span class="d-block font-weight-bolder">15:00</span>
                                                <span class="d-block font-weight-bolder">17:00</span>
                                            </div>
                                            <div class="col-2">
                                                <img class="rounded-circle cursor-pointer" src="http://localhost:8000/images/avatars/1.png" alt="avatar" height="46" width="46">
                                            </div>
                                            <div class="col-8">
                                                <span class="dblock font-weight-bolder">Kota Tua Session with Jessica</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="session-item">
                                <div class="divider divider-left">
                                    <div class="divider-text font-weight-bolder">Thursday, 24 April 2021 </div>
                                </div>
                                <div class="card">
                                    <div class="card-body border-left-primary">
                                        <div class="row g-0">
                                            <div class="col-2">
                                                <span class="d-block font-weight-bolder">15:00</span>
                                                <span class="d-block font-weight-bolder">17:00</span>
                                            </div>
                                            <div class="col-2">
                                                <img class="rounded-circle cursor-pointer" src="http://localhost:8000/images/avatars/1.png" alt="avatar" height="46" width="46">
                                            </div>
                                            <div class="col-8">
                                                <span class="dblock font-weight-bolder">Kota Tua Session with Jessica</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('vendor-script')
  <!-- Vendor js files -->
  <script src="{{ asset(mix('vendors/js/calendar/fullcalendar.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/schedule.js')) }}"></script>
  <script src="{{ asset(('js/scripts/member/calendar-event.js')) }}"></script>
@endsection
