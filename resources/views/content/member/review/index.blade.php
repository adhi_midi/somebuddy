@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Review')

@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.review') }}</h2>
</section>

<section class="mb-5">
  @if (!empty($reviews))
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col">
            <div class="list-review">
              @foreach ($reviews as $key => $item)
                @if ($key != 0)
                  <hr>
                @endif
                <div class="item-list">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="row">
                        <div class="col-md-3">
                          <img class="rounded-circle cursor-pointer" src="{{asset('images/profile/thumb/' . $item['image'])}}" alt="avatar" height="64" width="64">
                        </div>
                        <div class="col-md-6">
                          <h3 class="font-weight-bolder text-primary m-0">{{$item['name']}}</h3>
                          <p>{{$item['service']}}</p>
                        </div>
                        <div class="col-md-3">
                          <div class="d-flex align-items-center mt-25">
                            <i data-feather="star" class="text-warning feather-20"></i>
                            <span class="font-weight-bolder ml-50 font-medium-5 mt-1"><p>{{$item['rating']}}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8">
                      <p>{{$item['desc']}}</p>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  @else
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 text-center">
            <div class="divider w-25 d-inline-block">
                <div class="divider-text">{{ __('locale.reviews_empty') }}</div>
            </div>
        </div>
      </div>
    </div>
  @endif
</section>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/dashboard.js')) }}"></script>
@endsection
