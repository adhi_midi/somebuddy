@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Settings')

@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">Add New Service</h2>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label class="form-label" for="activity">Activity</label>
                <select name="activity" class="select2 form-control input-activity" data-minimum-results-for-search="Infinity">
                  @foreach($activities as $key => $data)
                    @if(in_array($data->id, $existActivities)))
                      <option value="{{ $data->id }}" disabled>{{ $data->name }}</option>
                    @else
                      <option value="{{ $data->id }}">{{ $data->name }}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <label class="form-label" for="price">Price</label>
              <select name="price" class="select2 form-control input-price" data-minimum-results-for-search="Infinity">
                @foreach($prices as $key => $data)  
                  <option value="{{ $data->id }}">Rp. {{ number_format($data->name, 0, ',', '.') }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-4">
              <label class="form-label" for="participant">Max Participant</label>
              <input id="participant" type="text" class="form-control input-participant @error('participant') is-invalid @enderror" name="participant" autocomplete="participant" required autofocus >
            </div>
          </div>
          <hr class="mb-2">
          <div class="schedule-list"></div>
          <div class="row mb-2">
            <div class="col-md-12 text-center">
              <button class="btn btn-outline-secondary btn-block btn-add-schedule">Add Schedule</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mb-2">
    <div class="col-md-12 text-center">
      <button class="btn btn-success btn-block btn-create-service">Create</button>
    </div>
  </div>
</section>

<div class="hidden">
  <div class="input-schedule">
    <div class="card border schedule-item">
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label" for="day">Days</label>
              <div class="input-days">
                <label><input type="checkbox" name="days" value="1"><span>Senin</span></label>
                <label><input type="checkbox" name="days" value="2"><span>Selasa</span></label>
                <label><input type="checkbox" name="days" value="3"><span>Rabu</span></label>
                <label><input type="checkbox" name="days" value="4"><span>Kamis</span></label>
                <label><input type="checkbox" name="days" value="5"><span>Jumat</span></label>
                <label><input type="checkbox" name="days" value="6"><span>Sabtu</span></label>
                <label><input type="checkbox" name="days" value="7"><span>Minggu</span></label>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <label class="form-label" for="start_time">Start Time</label>
              <select name="start_time" class="select2 form-control input-start-time" data-minimum-results-for-search="Infinity">
                @foreach($times as $key => $time)
                <option value="{{ Carbon\Carbon::parse($time->time)->format('H:i') }}">{{ Carbon\Carbon::parse($time->time)->format('H:i') }}</option>
                @endforeach
              </select>
          </div>
          <div class="col-md-2">
            <label class="form-label" for="end_time">End Time</label>
              <select name="end_time" class="select2 form-control input-end-time" data-minimum-results-for-search="Infinity">
                @foreach($times as $key => $time)
                <option value="{{ Carbon\Carbon::parse($time->time)->format('H:i') }}">{{ Carbon\Carbon::parse($time->time)->format('H:i') }}</option>
                @endforeach
              </select>
          </div>
          <div class="col-md-2 text-right">
            <div class="btn btn-md btn-flat-primary mb-50 btn-delete-schedule">Delete</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/settings/add.js')) }}"></script>
@endsection