@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Settings')

@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.settings') }}</h2>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row"></div>
          <div class="row mb-1">
            <div class="col-md-12 service-list">
              @if(!empty($userService))
                @foreach($userService as $key => $data)
                  <div class="card border">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-10">
                          <div class="row">
                            <div class="col-md-12">
                              <p class="font-weight-bolder font-medium-5">
                                @foreach($activities as $activity)
                                  @if ($data->activity_id == $activity->id)
                                    {{ $activity->name }}
                                  @endif
                                @endforeach
                              </p>
                            </div>
                          </div>
                          <div class="row mb-1">
                            <div class="col-md-6 col-lg-4">
                              <p class="d-inline">Tarif: </p>
                              <p class="d-inline font-weight-bolder">Rp.</p>
                              <p class="d-inline font-weight-bolder">
                                @foreach($prices as $price)
                                  @if ($data->price == $price->id)
                                    {{ number_format($price->name, 0, ',', '.') }}
                                  @endif
                                @endforeach
                              </p>
                              <p class="d-inline">/jam</p>
                            </div>
                            <div class="col-md-6 col-lg-4">
                              <p class="d-inline">Max Participant: </p>
                              <p class="d-inline font-weight-bolder">{{$data->participant}}</p>
                            </div>
                          </div>
                          <hr>
                          <div class="row mb-1">
                            <div class="col-md-8">
                              <p class="mb-25 w-100">Days</p>
                              @if(!empty($data->days))
                                @foreach(array_map('intval', json_decode($data->days) ?? []) as $day)
                                  <div class="btn btn-md btn-outline-primary disabled round mb-50">
                                    @switch($day)
                                      @case(1)
                                        Senin
                                        @break

                                      @case(2)
                                        Selasa
                                        @break

                                      @case(3)
                                        Rabu
                                        @break

                                      @case(4)
                                        Kamis
                                        @break

                                      @case(5)
                                        Jumat
                                        @break

                                      @case(6)
                                        Sabtu
                                        @break

                                      @case(7)
                                        Minggu
                                        @break

                                    @endswitch
                                  </div>
                                @endforeach
                              @else
                                -
                              @endif
                            </div>
                            <div class="col-md-2">
                              <p class="mb-25">Start Time</p>
                              <p class="font-weight-bolder font-medium-1">{{ Carbon\Carbon::parse($data->start_time)->format('H:i') }}</p>
                            </div>
                            <div class="col-md-2">
                              <p class="mb-25">End Time</p>
                              <p class="font-weight-bolder font-medium-1">{{ Carbon\Carbon::parse($data->end_time)->format('H:i') }}</p>
                            </div>
                          </div>
                          <div class="row">
                            
                          </div>
                        </div>
                        <div class="col-md-2 text-right">
                          <div class="btn btn-md btn-flat-primary mb-50" data-toggle="modal" data-target="#modal-service-update">Edit</div>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              @else
                -
              @endif
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-md-12 text-center">
              <button class="btn btn-success btn-block" data-toggle="modal" data-target="#modal-service">Add New</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div 
  class="modal fade text-left modal-service-update" 
  id="modal-service-update"
  tabindex="-1"
  role="dialog"
  aria-labelledby="modal-label"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-label">Service Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>

<div 
  class="modal fade text-left modal-service" 
  id="modal-service"
  tabindex="-1"
  role="dialog"
  aria-labelledby="modal-label"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-label">Add New Service</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="POST" enctype="multipart/form-data" action="{{route('service-add')}}">
          @csrf  
          <div class="form-group">
            <label class="form-label" for="activity">Service</label>
            <select name="activity" class="select2 form-control" data-minimum-results-for-search="Infinity">
              @foreach($activities as $key => $data)  
                <option value="{{ $data->id }}">{{ $data->name }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group row">
            <div class="col-md-6">
              <label class="form-label" for="price">Price</label>
              <select name="price" class="select2 form-control" data-minimum-results-for-search="Infinity">
                @foreach($prices as $key => $data)  
                  <option value="{{ $data->id }}">Rp. {{ number_format($data->name, 0, ',', '.') }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-6">
              <label class="form-label" for="participant">Max Participant</label>
              <input id="participant" type="text" class="form-control @error('participant') is-invalid @enderror" name="participant" autocomplete="participant" required autofocus >
            </div>
          </div>

          <div class="form-group">
            <label class="form-label" for="day">Days</label>
            <select name="days[]" class="select2 form-control" data-minimum-results-for-search="Infinity" multiple>
              <option value="1">Senin</option>
              <option value="2">Selasa</option>
              <option value="3">Rabu</option>
              <option value="4">Kamis</option>
              <option value="5">Jumat</option>
              <option value="6">Sabtu</option>
              <option value="7">Minggu</option>
            </select>
          </div>

          <div class="form-group row">
            <div class="col-md-6">
              <label class="form-label" for="start_time">Start Time</label>
              <select name="start_time" class="select2 form-control" data-minimum-results-for-search="Infinity">
                <option value="00:00">00:00</option>
                <option value="01:00">01:00</option>
                <option value="02:00">02:00</option>
                <option value="03:00">03:00</option>
                <option value="04:00">04:00</option>
                <option value="05:00">05:00</option>
                <option value="06:00">06:00</option>
                <option value="07:00">07:00</option>
                <option value="08:00">08:00</option>
                <option value="09:00">09:00</option>
                <option value="10:00">10:00</option>
                <option value="11:00">11:00</option>
                <option value="12:00">12:00</option>
                <option value="13:00">13:00</option>
                <option value="14:00">14:00</option>
                <option value="15:00">15:00</option>
                <option value="16:00">16:00</option>
                <option value="17:00">17:00</option>
                <option value="18:00">18:00</option>
                <option value="19:00">19:00</option>
                <option value="20:00">20:00</option>
                <option value="21:00">21:00</option>
                <option value="22:00">22:00</option>
                <option value="23:00">23:00</option>
                <option value="24:00">24:00</option>
              </select>
            </div>
            <div class="col-md-6">
              <label class="form-label" for="end_time">End Time</label>
              <select name="end_time" class="select2 form-control" data-minimum-results-for-search="Infinity">
                <option value="00:00">00:00</option>
                <option value="01:00">01:00</option>
                <option value="02:00">02:00</option>
                <option value="03:00">03:00</option>
                <option value="04:00">04:00</option>
                <option value="05:00">05:00</option>
                <option value="06:00">06:00</option>
                <option value="07:00">07:00</option>
                <option value="08:00">08:00</option>
                <option value="09:00">09:00</option>
                <option value="10:00">10:00</option>
                <option value="11:00">11:00</option>
                <option value="12:00">12:00</option>
                <option value="13:00">13:00</option>
                <option value="14:00">14:00</option>
                <option value="15:00">15:00</option>
                <option value="16:00">16:00</option>
                <option value="17:00">17:00</option>
                <option value="18:00">18:00</option>
                <option value="19:00">19:00</option>
                <option value="20:00">20:00</option>
                <option value="21:00">21:00</option>
                <option value="22:00">22:00</option>
                <option value="23:00">23:00</option>
                <option value="24:00">24:00</option>
              </select>
            </div>
          </div>

          <button class="btn btn-primary my-2" type="submit">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/settings.js')) }}"></script>
@endsection
