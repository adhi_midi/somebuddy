@extends('layouts/contentLayoutMaster')
@section('title', 'Member Area - Settings')

@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.settings') }}</h2>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row pt-2 mb-4">
            <div class="col-md-4">
              <div class="img-profile text-center">

                @if(!empty($profile['image']))
                  <img class="rounded-circle cursor-pointer" src="{{asset('images/profile/' . $profile['image'])}}" alt="avatar" height="240" width="240">
                @else
                  <img class="rounded-circle cursor-pointer" src="{{asset('images/profile/default_avatar.png')}}" alt="avatar" height="240" width="240">
                @endif
            
                <!-- <button type="button" class="btn btn-outline-primary mt-2 w-75 mb-1" data-toggle="modal" data-target="#modal-photo">
                  Change Photo
                </button> -->

                <button type="button" class="btn btn-outline-primary mt-2 w-75 mb-1 btn-change-photo" id="select-files-profile">
                  Change Photo
                </button>

                <form class="dzProfile hidden" id="dzProfile" enctype="multipart/form-data">
                  @csrf 
                  <div class="fallback">
                    <input name="file" type="file"/>
                  </div>
                </form>

                <button class="btn btn-outline-secondary w-75 mb-3" data-toggle="modal" data-target="#modal-password">Change Password</button>
                @role('member')
                <button class="btn btn-success w-75 mb-3 btn-upgrade-member">Upgrade to Buddy</button>
                @endrole
              </div>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-6">
                  <p class="mb-0 mt-25 font-weight-bolder">Profile Detail</p>
                </div>
                <div class="col-6 text-right">
                  <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#modal-profile">Edit</button>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-4 col-md-3">
                  <p class="font-weight-bold">Name</p>
                  <p class="font-weight-bold">Birthdate</p>
                  <p class="font-weight-bold">Language</p>
                  <p class="font-weight-bold">Gender</p>
                  <p class="font-weight-bold">Contact</p>
                  <p class="font-weight-bold">Nationality</p>
                  <p class="font-weight-bold">Location</p>
                </div>
                <div class="col">
                  <p class="text-primary font-weight-bold">{{ $profile['name'] }}</p>
                  <p class="text-primary">
                    @if(!empty($profile['birthdate']))
                      {{ $profile['birthdate'] }}
                    @else
                      -
                    @endif
                  </p>
                  <p class="text-primary">
                    @if(!empty($profile['language']))
                      @foreach($profile['language'] as $key => $data)
                        @if($key == 0)
                          {{$data}}
                        @else
                          , {{$data}}
                        @endif
                      @endforeach
                    @else
                      -
                    @endif
                  </p>
                  <p class="text-primary">
                    @if(!empty($profile['gender']))
                      {{ $profile['gender'] }}
                    @else
                      -
                    @endif
                  </p>
                  <p class="text-primary">
                    @if(!empty($profile['phone']))
                      {{ $profile['phone'] }}
                    @else
                      -
                    @endif
                  </p>
                  <p class="text-primary">
                    @if(!empty($profile['nationality']))
                      {{ $profile['nationality'] }}
                    @else
                      -
                    @endif
                  </p>
                  <p class="text-primary">
                    @if(!empty($profile['location']))
                      {{ $profile['location'] }}
                    @else
                      -
                    @endif
                  </p>
                </div>
              </div>
              <div class="row mt-1">
                <div class="col-md-12">
                  <p class="font-weight-bolder">About Me</p>
                  <p>
                  @if(!empty($profile['about']))
                    {{ $profile['about'] }}
                  @else
                      -
                  @endif
                  </p>
                </div>
                <div class="col-md-12 mt-2">
                  <p class="font-weight-bolder">Activity</p>
                  <div class="list-activity">
                    @if(!empty($profile['activity']))
                      @foreach($profile['activity'] as $key => $data)
                        <div class="btn btn-md btn-outline-primary disabled round mb-50">{{$data}}</div>
                      @endforeach
                    @else
                      -
                    @endif
                  </div>
                </div>
                <div class="col-md-12 mt-2">
                  <p class="font-weight-bolder">Interest</p>
                  <div class="list-interest">
                    @if(!empty($profile['interest']))
                      @foreach($profile['interest'] as $key => $data)
                      <div class="btn btn-md btn-outline-primary disabled round mb-50">{{$data}}</div>
                      @endforeach
                    @else
                      -
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12 text-center mb-2 mt-1">
              <p class="font-medium-3 font-weight-bolder">My Gallery</p>
            </div>
          </div>
          <div class="row setting-galleries">
            @foreach($galleries as $key => $data)
            <div class="col-md-2">
              <a href="{{asset('images/gallery/' . $data['image'])}}" data-fslightbox>
                <div class="img imgLiquidFill imgLiquid item-gallery mb-2">
                  <img src="{{asset('images/gallery/' . $data['image'])}}" alt="img-placeholder">
                </div>
              </a>
            </div>
            @endforeach
            
            <div class="col-md-2">
              <div class="img imgLiquidFill imgLiquid item-gallery btn-add-image-gallery border mb-2" id="select-files-gallery">
                <i data-feather="plus" height="30" width="30"></i>
              </div>
              <form class="dzGallery hidden" id="dzGallery" enctype="multipart/form-data">
                @csrf 
                <div class="fallback">
                  <input name="file" type="file"/>
                </div>
              </form>
            </div>
          </div>
          @role('buddy')
          <hr>
          <div class="row">
            <div class="col-md-12 text-center mb-2 mt-1">
              <p class="font-medium-3 font-weight-bolder">My Services</p>
            </div>
            <div class="col-md-12 service-list">
              @if(!empty($services))
                @foreach($services as $key => $data)
                  <div class="card border">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-10">
                          <div class="row">
                            <div class="col-md-12">
                              <p class="font-weight-bolder font-medium-5">
                                {{ $data['activity'] }}
                              </p>
                            </div>
                          </div>
                          <div class="row mb-1">
                            <div class="col-md-6 col-lg-4">
                              <p class="d-inline">Tarif: </p>
                              <p class="d-inline font-weight-bolder">Rp.</p>
                              <p class="d-inline font-weight-bolder">{{ $data['price'] }}</p>
                              <p class="d-inline">/jam</p>
                            </div>
                            <div class="col-md-6 col-lg-4">
                              <p class="d-inline">Max Participant: </p>
                              <p class="d-inline font-weight-bolder">{{ $data['participant'] }}</p>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2 text-right">
                          <a class="btn btn-md btn-flat-primary mb-50" href="{{ url('settings/services/'.$data['service_id']) }}">Edit</a>
                        </div>
                      </div>
                      
                      <div class="service-schedules">
                        @foreach($data['schedules'] as $key => $schedule)
                        <hr>
                        <div class="row mb-1">
                          <div class="col-md-6">
                            <p class="mb-25 w-100">Days</p>
                            @foreach($schedule['days'] as $key => $day)
                            <div class="btn btn-md btn-outline-primary disabled round mb-50">{{$day}}</div>
                            @endforeach
                          </div>
                          <div class="col-md-2">
                            <p class="mb-25">Start Time</p>
                            <p class="font-weight-bolder font-medium-1">{{ $schedule['start_time'] }}</p>
                          </div>
                          <div class="col-md-2">
                            <p class="mb-25">End Time</p>
                            <p class="font-weight-bolder font-medium-1">{{ $schedule['end_time'] }}</p>
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                @endforeach
              @else
                -
              @endif
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-md-12 text-center">
              <a class="btn btn-success btn-block btn-create-service" href="{{ route('service-create'); }}">Add New Service</a>
            </div>
          </div>
          @endrole
        </div>
      </div>
    </div>
  </div>
</section>

<div 
  class="modal fade text-left modal-profile" 
  id="modal-profile"
  tabindex="-1"
  role="dialog"
  aria-labelledby="modal-label"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-label">Edit Profile Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data" action="{{route('update-profile')}}">
          @csrf  
          <div class="form-group row">
            <div class="col-md-6">
              <label class="form-label" for="name">Name</label>
              <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" autocomplete="name" required autofocus value="@if(!empty($user->name)){{$user->name}}@endif">

              @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label class="form-label" for="birthdate">Birthdate</label>
              <input id="birthdate" type="text" class="form-control flatpickr-date @error('birthdate') is-invalid @enderror" name="birthdate" value="{{ old('birthdate') }}" autocomplete="birthdate" autofocus data-date="@if(!empty($user->birthdate)) {{ Carbon\Carbon::parse($user->birthdate)->format('d-m-Y') }} @else - @endif" required>

              @error('birthdate')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-6">
              <label class="form-label" for="language">Language</label>
              <select name="language[]" class="select2 form-control" data-minimum-results-for-search="Infinity" multiple required>
                @foreach($languages as $key => $data)  
                  @if(in_array($data->id, array_map('intval', json_decode($user->language) ?? [])))
                    <option value="{{ $data->id }}" selected>{{ $data->name }}</option>
                  @else
                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="col-md-6">
              <label class="form-label">Gender</label>
              <select name="gender" class="select2 form-control" data-minimum-results-for-search="Infinity" required>
                @foreach($genders as $key => $data)
                  @if ($user->gender == $data->id)
                    <option value="{{ $data->id }}" selected>{{ $data->name }}</option>
                  @else
                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-6">
              <label class="form-label">Phone Number</label>
              <input id="phone_number" type="tel" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="@if(!empty($user->phone_number)){{$user->phone_number}}@endif" required autocomplete="phone_number" autofocus>

              @error('phone_number')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label class="form-label">Nationality</label>
              <select name="nationality" class="select2 form-control" data-minimum-results-for-search="Infinity" required>
                @foreach($nationalities as $key => $data)
                  @if ($user->nationality == $data->id)
                    <option value="{{ $data->id }}" selected>{{ $data->name }}</option>
                  @else
                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-6">
              <label class="form-label">Location</label>
              <select name="location" class="select2 form-control" required>
                @foreach($cities as $key => $data)
                  @if ($user->location == $data->city_id)
                    <option value="{{ $data->city_id }}" selected>{{ $data->city_name }}</option>
                  @else
                    <option value="{{ $data->city_id }}">{{ $data->city_name }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="form-label">About Me</label>
            <textarea class="form-control" rows="3" name="about" required>@if(!empty($user->about)){{ $user->about }}@endif</textarea>
          </div>

          <div class="form-group">
            <label class="form-label">Activity</label>  
            <select name="activity[]" class="select2 form-control" multiple required>
                @foreach($activities as $key => $data)  
                  @if(in_array($data->id, array_map('intval', json_decode($user->activity) ?? []) ))
                    <option value="{{ $data->id }}" selected>{{ $data->name }}</option>
                  @else
                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                  @endif
                @endforeach
              </select>
          </div>

          <div class="form-group">
            <label class="form-label">Interest</label>  
            <select name="interest[]" class="select2 form-control" multiple required>
                @foreach($interests as $key => $data)  
                  @if(in_array($data->id, array_map('intval', json_decode($user->interest) ?? []) ))
                    <option value="{{ $data->id }}" selected>{{ $data->name }}</option>
                  @else
                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                  @endif
                @endforeach
              </select>
          </div>

          <button class="btn btn-primary my-2" type="submit">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div 
  class="modal fade text-left modal-photo" 
  id="modal-photo"
  tabindex="-1"
  role="dialog"
  aria-labelledby="modal-label"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-label">Edit Photo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data" id="photo-file" action="{{route('update-profile-image')}}">
        @csrf 
          <div class="form-group">
            <label class="form-label">Max. photo file size 500kb</label>
              <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" required autocomplete="image" autofocus>

              @error('image')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
          <button class="btn btn-primary my-2" type="submit">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div 
  class="modal fade text-left modal-password" 
  id="modal-password"
  tabindex="-1"
  role="dialog"
  aria-labelledby="modal-label"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-label">Change Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- <form method="POST" enctype="multipart/form-data" id="update-password" action="{{route('update-password')}}">
        @csrf  -->
          <div class="form-group">
            <label class="form-label" for="register-password">New Password</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          <div class="form-group">
            <label class="form-label" for="register-password">Confirm New Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"> 
          </div>
          <button class="btn btn-primary my-2 btn-update-password" >Save</button>
        <!-- </form> -->
      </div>
    </div>
  </div>
</div>

@endsection
@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/settings.js')) }}"></script>
@endsection
