@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Settings')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
  <!-- Page css files -->
@endsection

@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.settings') }}</h2>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs card-header-tabs ms-0">
            <li class="nav-item">
              <a class="nav-link" href="{{route('profile-setting')}}">Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('gallery-setting')}}">Gallery</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('service-setting')}}">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="{{route('payment-setting')}}">Rekening Bank</a>
            </li>
          </ul>
        </div>
        <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card border">
              <div class="card-body">
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(('js/scripts/components/components-modals.js')) }}"></script>
  <script src="{{ asset(('js/scripts/member/settings.js')) }}"></script>
@endsection
