@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Wallet')

@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.withdraw') }}</h2>
</section>

<section>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="d-flex mb-1">
            <i data-feather="dollar-sign" class="feather-25" height="40" width="40"></i>
            <span class="font-medium-3 ml-50">{{ __('locale.your_balance') }}</span>
          </div>
          <span class="font-large-1 font-weight-bolder text-primary mb-50">Rp {{$balance}}</span>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/wallet.js')) }}"></script>
@endsection
