@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Wallet')

@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.wallet') }}</h2>
</section>

<section>
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <div class="d-flex mb-1">
            <i data-feather="dollar-sign" class="feather-25" height="40" width="40"></i>
            <span class="font-medium-3 ml-50">{{ __('locale.your_balance') }}</span>
          </div>
          <span class="font-large-1 font-weight-bolder text-primary mb-50">Rp {{$balance}}</span>
          <hr class="my-2">
          <div class="d-flex">
            <a href="{{route('wallet-withdraw')}}" class="btn btn-success w-50 px-1">{{ __('locale.withdraw') }}</a>
            <a href="{{route('wallet-deposit')}}" class="btn btn-primary w-50 px-1 ml-1">{{ __('locale.deposit') }}</a>
          </div>
          <a href="{{route('wallet-bank')}}" class="btn btn-light-primary w-100 mt-1">{{ __('locale.add_bank') }}</a>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
            <div class="row">
              <div class="col-6">
                <div class="d-flex pt-50">
                  <i data-feather="pocket" class="feather-25" height="40" width="40"></i>
                  <span class="font-medium-3 ml-50">{{ __('locale.history_balance') }}</span>
                </div>
              </div>
              <div class="col-6 form-group mb-0">
                <div class="d-flex">
                  <i data-feather="calendar" class="feather-25 mt-50 mr-1" height="40" width="40"></i>
                  <input type="text" id="fp-range" class="form-control flatpickr-range flatpickr-input active" placeholder="YYYY-MM-DD to YYYY-MM-DD" readonly="readonly">
                </div>
              </div>
            </div>
          <hr class="mt-2 mb-0">
          <ul class="list-group list-group-flush">
            <li class="list-group-item">
              <div class="row">
                <div class="col-md-6 text-center text-md-left">
                  <p class="font-weight-bolder font-medium-3 text-primary mb-50">
                    Julian Jacob
                  </p>
                  <p class="m-0">22/03/2021</p>
                  <p class="m-0 font-weight-bolder">3 Jam</p>
                  <hr class="d-md-none">
                </div>
                <div class="col-md-6 text-center text-md-right">
                  <p class="font-weight-bolder font-medium-3 text-primary mb-50">
                    + Rp 270.000
                  </p>
                  <p class="m-0"><small>(Tarif)</small> <b>+ Rp 300.000</b></p>
                  <p class="m-0"><small>(Fee)</small> <b>- Rp 30.000</b></p>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/wallet.js')) }}"></script>
@endsection
