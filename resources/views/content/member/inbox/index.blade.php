@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Inbox')

@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(('css/base/pages/app-chat.css')) }}">
  <link rel="stylesheet" href="{{ asset(('css/base/pages/app-chat-list.css')) }}">
@endsection
{{-- @include('content/member/inbox/app-chat-sidebar') --}}
@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
{{-- <section>
  <h2 class="page-title font-bolder text-primary">{{ __('locale.inbox') }}</h2>
</section> --}}

<section>
  <div class="content-area-wrapper container-xxl p-0">
    <div class="sidebar-left">
      <div class="sidebar">
        <div class="chat-profile-sidebar">
          <span class="close-icon">
            <i data-feather="x"></i>
          </span>
        </div>
        <div class="sidebar-content card">
          <span class="sidebar-close-icon">
            <i data-feather="x"></i>
          </span>
          <div class="sidebar-title px-2 pt-2 pb-1">
            <h4 class="text-primary font-weight-bolder">{{ __('locale.inbox') }}</h4>
          </div>
          {{-- <div class="chat-fixed-search">
            <div class="d-flex align-items-center w-100">
              <div class="input-group input-group-merge w-100">
                <div class="input-group-prepend">
                  <span class="input-group-text round" id="chat-search">
                    <i data-feather="search"></i>
                  </span>
                </div>
                <input
                  type="text"
                  class="form-control round"
                  placeholder="Search..."
                  aria-label="Search..."
                  aria-describedby="chat-search"
                />
              </div>
            </div>
          </div> --}}
          <div id="users-list" class="chat-user-list-wrapper list-group ps ps--active-y">
            <ul class="chat-users-list chat-list media-list">
              <!-- <li>
                <span class="avatar"><img src="{{asset('images/avatars/1.png')}}" height="42" width="42">
                  <span class="avatar-status-offline"></span>
                </span>
                <div class="chat-info flex-grow-1">
                  <h5 class="mb-0">Candy</h5>
                  <p class="card-text text-truncate">
                    Lorem Ipsum
                  </p>
                </div>
                <div class="chat-meta text-nowrap">
                  <small class="float-end mb-25 chat-time">4:14 PM</small>
                </div>
              </li> -->
              
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="content-right">
      <section class="chat-app-window">
        <!-- Active Chat -->
        <div class="active-chat">
          <!-- Chat Header -->
          <div class="chat-navbar">
            <!-- <header class="chat-header">
              <div class="d-flex align-items-center">
                <div class="sidebar-toggle d-block d-lg-none mr-1">
                  <i data-feather="menu" class="font-medium-5"></i>
                </div>
                <div class="avatar avatar-border user-profile-toggle m-0 mr-1">
                  <img src="{{ asset('images/avatars/1.png') }}" alt="avatar" height="36" width="36" />
                  <span class="avatar-status-busy"></span>
                </div>
                <h6 class="mb-0">{{ Auth::user()->name }}</h6>
              </div>
              <div class="d-flex align-items-center">
                <div class="dropdown">
                  <button
                    class="btn-icon btn btn-transparent hide-arrow btn-sm dropdown-toggle"
                    type="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i data-feather="more-vertical" id="chat-header-actions" class="font-medium-2"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="chat-header-actions">
                    <a class="dropdown-item" href="javascript:void(0);">View Profile</a>
                  </div>
                </div>
              </div>
            </header> -->
          </div>
          <!--/ Chat Header -->
      
          <!-- User Chat messages -->
          <!-- <div class="user-chats">
            <div class="chats">
              <div class="chat chat-left">
                <div class="chat-avatar">
                  <span class="avatar box-shadow-1 cursor-pointer">
                    <img src="{{ asset('images/avatars/1.png') }}" height="36" width="36" />
                  </span>
                </div>
                <div class="chat-body">
                  <div class="chat-content">
                    <p>Hey John, Are you available on saturday?</p>
                  </div>
                </div>
              </div>
              <div class="chat">
                <div class="chat-avatar">
                  <span class="avatar box-shadow-1 cursor-pointer">
                    <img
                      src="{{ asset('images/avatars/2.png') }}"
                      height="36"
                      width="36"
                    />
                  </span>
                </div>
                <div class="chat-body">
                  <div class="chat-content">
                    <p>Absolutely!</p>
                  </div>
                </div>
              </div>
              <div class="divider">
                <div class="divider-text">Yesterday</div>
              </div>
              <div class="chat chat-left">
                <div class="chat-avatar">
                  <span class="avatar box-shadow-1 cursor-pointer">
                    <img src="{{ asset('images/avatars/1.png') }}" height="36" width="36" />
                  </span>
                </div>
                <div class="chat-body">
                  <div class="chat-content">
                    <p>Great. 😃</p>
                  </div>
                  <div class="chat-content">
                    <p>I will book it for sure.</p>
                  </div>
                </div>
              </div>
              <div class="chat">
                <div class="chat-avatar">
                  <span class="avatar box-shadow-1 cursor-pointer">
                    <img
                      src="{{ asset('images/avatars/2.png') }}"
                      height="36"
                      width="36"
                    />
                  </span>
                </div>
                <div class="chat-body">
                  <div class="chat-content">
                    <p>Great, Feel free to get in touch on</p>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
          <!-- User Chat messages -->
      
          <!-- Submit Chat form -->
          <!-- <form class="chat-app-form" action="javascript:void(0);" onsubmit="enterChat();">
            <div class="input-group input-group-merge mr-1 form-send-message">
              <input type="text" class="form-control message" placeholder="Type your message" />
            </div>
            <button type="button" class="btn btn-primary send" onclick="enterChat();">
              <i data-feather="send" class="d-lg-none"></i>
              <span class="d-none d-lg-block">Send</span>
            </button>
          </form> -->
          <!--/ Submit Chat form -->
        </div>
        <!--/ Active Chat -->
      </section>
    </div>
  </div>
</section>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/inbox.js')) }}"></script>
@endsection
