@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Transaction')
@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary mb-2">{{ __('locale.transaction') }}</h2>
</section>

<section>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <form>
            <div class="row">
                <div class="col-md-4 form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon-search"><i data-feather="search"></i></span>
                    </div>
                    <input
                      type="text"
                      class="form-control"
                      placeholder="Search..."
                      aria-label="Search..."
                      name="search"
                      aria-describedby="basic-addon-search"
                    />
                  </div>
                </div>
                <div class="col-md-4 form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon-calendar"><i data-feather="calendar"></i></span>
                    </div>
                    
                    <input
                      type="text"
                      id="fp-range"
                      class="form-control flatpickr-range"
                      placeholder="Select Date.."
                      name="date"
                      aria-describedby="basic-addon-calendar"
                    />
                  </div>
                </div>
                <div class="col-md-4">
                  <input type="text" name="status" value="{{$status}}" class="hidden">
                  <button class="btn btn-primary btn-search" type="submit">Search</button>
                </div>
            </div>
          </form>
          <div class="filter-status d-infline-flex">
            <div class="d-inline-block mr-1">Status:</div>
            <ul class="filter-status-list list-inline d-inline-block mb-0">
                <li>
                  @if ($status == 'new')
                    <a href="{!! url('/transaction?status=new'); !!}" class="btn btn-primary mr-25">{{ __('locale.new_order') }}</a>
                  @else
                    <a href="{!! url('/transaction?status=new'); !!}" class="btn btn-outline-primary mr-25">{{ __('locale.new_order') }}</a>
                  @endif
                </li>
                <li>
                  @if ($status == 'ongoing')
                    <a href="{!! url('/transaction?status=ongoing'); !!}" class="btn btn-primary mr-25">{{ __('locale.ongoing') }}</a>
                  @else
                    <a href="{!! url('/transaction?status=ongoing'); !!}" class="btn btn-outline-primary mr-25">{{ __('locale.ongoing') }}</a>
                  @endif
                </li>
                <li>
                  @if ($status == 'complain')
                    <a href="{!! url('/transaction?status=complain'); !!}" class="btn btn-primary mr-25">{{ __('locale.in_complain') }}</a>
                  @else
                    <a href="{!! url('/transaction?status=complain'); !!}" class="btn btn-outline-primary mr-25">{{ __('locale.in_complain') }}</a>
                  @endif
                </li>
                <li>
                  @if ($status == 'done')
                    <a href="{!! url('/transaction?status=done'); !!}" class="btn btn-primary mr-25">{{ __('locale.done') }}</a>
                  @else
                    <a href="{!! url('/transaction?status=done'); !!}" class="btn btn-outline-primary mr-25">{{ __('locale.done') }}</a>
                  @endif
                </li>
                <li>
                  @if ($status == 'all')
                    <a href="{!! url('/transaction?status=all'); !!}" class="btn btn-primary mr-25">{{ __('locale.all') }}</a>
                  @else
                    <a href="{!! url('/transaction?status=all'); !!}" class="btn btn-outline-primary mr-25">{{ __('locale.all') }}</a>
                  @endif
                </li>
            </ul>
          </div>
          <hr>
          <div class="list-transaction">
            <ul class="list-group list-group-flush">
              @foreach ($transactions as $item)
                <li class="list-group-item">
                  <div class="row mb-2">
                    <div class="col-md-12">
                      <span class="font-weight-bold text-primary mr-2">{{ $item['service_name'] }}</span>
                      <span class="mr-2">{{$item['date']}}</span>
                      <span class="font-weight-bold float-right">{{$item['invoice_no']}}</span>
                      @switch($item['status'])
                            @case(1)
                              <span class="badge badge-light-warning mr-2 float-right">{{ __('locale.waiting_for_payment') }}</span>
                                @break
                
                            @case(2)
                              <span class="badge badge-light-warning mr-2 float-right">{{ __('locale.waiting_for_payment') }}</span>
                                @break

                            @case(3)
                              <span class="badge badge-light-primary mr-2 float-right">{{ __('locale.waiting_confirmation') }}</span>
                                @break

                            @case(4)
                              <span class="badge badge-light-success mr-2 float-right">{{ __('locale.ongoing') }}</span>
                                @break

                            @case(5)
                              <span class="badge badge-light-secondary mr-2 float-right">{{ __('locale.done') }}</span>
                                @break

                            @case(6)
                              <span class="badge badge-light-warning mr-2 float-right">{{ __('locale.in_complain') }}</span>
                                @break
                
                        @endswitch
                      
                    </div>
                    <div class="col-md-9">
                    </div>
                    <div class="col-md-3">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1">
                      <img class="rounded-circle cursor-pointer" src="{{asset('images/profile/thumb/' . $item['image'])}}" alt="avatar" height="64" width="64">
                    </div>
                    <div class="col-md-8">
                      <span class="d-block font-medium-2 font-weight-bolder text-primary">{{$item['name']}}</span>
                      <span class="d-block font-small-3">{{$item['start_time']}} - {{$item['end_time']}}</span>
                      <span class="d-block font-small-3">{{$item['duration']}} {{ __('locale.hour') }}, {{$item['participant']}} {{ __('locale.participant') }}</span>
                    </div>
                    <div class="col-md-3 text-right">
                      <span class="d-block font-small-3">Total</span>
                      <span class="d-block font-medium-3 font-weight-bolder text-primary">{{ number_format($item['total_price'], 0, ',', '.') }}</span>
                    </div>
                    <div class="col-md-12 text-right">
                      <a href="#" class="font-small-2 mt-2">View Detail</a>
                    </div>
                  </div>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/transaction.js')) }}"></script>
@endsection
