@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Dashboard')

@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary">{{ __('locale.dashboard') }}</h2>
  <p>{{ Carbon\Carbon::now()->timezone('Asia/Jakarta')->format('l, d M Y') }}</p>
</section>
<section class="mt-3">
  <div class="row">
    <div class="col-md-6">
      <div class="card dash-wallet">
        <div class="card-body">
          <div class="d-flex mb-1">
            <i data-feather="pocket" class="feather-20" height="40" width="40"></i>
            <span class="ml-50">{{ __('locale.balance') }}</span>
          </div>
          <span class="font-large-2 font-weight-bolder text-primary">Rp 0</span>
          <div class="w-100 text-right">
            <a href="wallet">
              <button class="btn mt-2"><span>{{ __('locale.more_detail') }}</span><i data-feather="chevron-right"></i></button>
            </a>
          </div>
        </div>
      </div>
      <div class="card dash-wallet-history">
        <div class="card-body">
          <div class="d-flex mb-1">
            <i data-feather="pocket" class="feather-20" height="40" width="40"></i>
            <span class="ml-50">{{ __('locale.transaction') }}</span>
          </div>
          <ul class="list-group list-group-flush">
            <!-- <li class="list-group-item">
              <div class="row">
                <div class="col-6">
                  <span class="font-medium-2 font-weight-bolder d-block">Ms. Sherly</span>
                  <span>25/03/21</span>
                </div>
                <div class="col-6">
                  <span class="font-medium-5 font-weight-bolder text-primary d-block text-right mt-25">+ Rp 300.000</span>
                </div>
              </div>
            </li> -->
          </ul>
          <!-- <div class="w-100 text-center">
            <a href="transaction">
              <button class="btn btn-flat-primary">{{ __('locale.view_more') }}</button>
            </a>
          </div> -->
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card dash-inbox">
        <div class="card-body">
          <div class="d-flex mb-1">
            <i data-feather="mail" class="feather-20" height="40" width="40"></i>
            <span class="ml-50">{{ __('locale.inbox') }}</span>
          </div>
          <div class="media-list media-bordered">
            <!-- <div class="media">
              <div class="media-left">
                <img src="{{asset('images/avatars/1.png')}}" alt="avatar" height="64" width="64" class="rounded-circle cursor-pointer">
              </div>
              <div class="media-body">
                <h4 class="media-heading">User 1</h4>
                Cookie candy dragée marzipan gingerbread pie pudding. 
              </div>
            </div> -->
          </div>
          <!-- <div class="w-100 text-center">
            <a href="inbox">
              <button class="btn btn-flat-primary">{{ __('locale.view_more') }}</button>
            </a>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/dashboard.js')) }}"></script>
@endsection
