@extends('layouts/contentLayoutMaster')

@section('title', 'Member Area - Wishlist')

@section('content')
<section>
  <div class="data-type" data-user-type="{{ Auth::user()->user_type }}"></div>
</section>
<section>
  <h2 class="page-title font-bolder text-primary">{{ __('locale.wishlist') }}</h2>
</section>

<section class="list-buddy mt-2 mb-5">
  <div class="row item-lists ">
    @if (!empty($wishlists))
        @foreach($wishlists as $key => $data)
            <div class="col-md-4">
                <div class="card">
                    <div class="item-list item-buddy">
                        <div class="img imgLiquidFill imgLiquid">
                            <img src="{{asset('images/profile/' . $data['user_image'])}}" alt="img-placeholder" style="display: none;">
                        </div>
                        <div class="item-content">
                            <div class="item-header mt-1">
                                <div class="row m-0">
                                    <div class="col">
                                        <div class="badge badge-primary">{{$data['service_name']}}</div>
                                    </div>
                                    <div class="col-2 text-right">
                                        @if ($data['wishlist'])
                                            <a href="javascript:void(0)" class="btn-wishlist active" data-service="{{$data['service_id']}}">
                                                <i data-feather="heart"></i>
                                            </a>
                                        @else
                                            <a href="javascript:void(0)" class="btn-wishlist" data-service="{{$data['service_id']}}">
                                                <i data-feather="heart"></i>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer position-absolute position-bottom-0 w-100 py-1">
                                <div class="row m-0">
                                    <div class="col">
                                        <a href="{{ url('buddies/' . $data['user_id']) }}">
                                            <h3 class="font-weight-bolder text-white m-0">{{$data['user_name']}}</h3>
                                        </a>
                                        <span>
                                            <i data-feather="map-pin" class="text-white"></i>
                                            <span class="text-white ml-25 font-small-1">{{$data['location']}}</span>
                                        </span>
                                    </div>
                                    <div class="col-4 text-right pt-2">
                                        <i data-feather="star" class="text-warning"></i>
                                        <span class="font-weight-bolder text-white ml-25">{{$data['rating']}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h4 class="d-block font-weight-bolder mb-25">Rp. {{number_format($data['price'], 0, ',', '.')}}</h4>
                                <p class="m-0">/ {{ __('locale.hour') }}</p>
                            </div>
                            <div class="col-5 text-right">
                                <a href="{{ url('booking/' . $data['service_id']) }}" class="btn btn-primary" data-service="{{$data['service_id']}}">Book</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="col-md-12">
            <div class="col-md-12 text-center">
                <div class="divider w-25 d-inline-block">
                    <div class="divider-text">{{ __('locale.wishlist_empty') }}</div>
                </div>
            </div>
        </div>
    @endif
  </div>
</section>

@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/member/wistlist.js')) }}"></script>
@endsection
