@extends('layouts/indexBuddyLayoutMaster')

@section('title', 'Buddies')

@section('content')
<!-- Page layout -->
<section class="booking-hero" style="background-image: url('{{asset('images/banners/' . $heroBanner['image'])}}');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="font-large-2 text-white title">{{ __('locale.booking') }}</h3>
            </div>
        </div>
    </div>
</section>

<section class="booking-detail mb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="card card-booking" style="min-height:500px">
                    <div class="card-body">
                        <div class="row service-profile mb-3" data-price="{{$service['price']}}" data-service={{$service['id']}}>
                            <div class="col-md-3">
                                <div class="img-profile text-center">
                                    <img class="rounded-circle box-shadow-1" src="{{asset('images/profile/' . $service['user_image'])}}" alt="avatar" height="200" width="200">
                                </div>
                            </div>
                            <div class="col-md-9">
                                <p class="font-large-1 font-weight-bolder title">
                                    {{$service['activity_name']}}
                                </p>
                                <p class="font-medium-4 font-weight-bolder">
                                    <span class="">{{ __('locale.with') }} </span>
                                    <span class="text-primary">{{$service['user_name']}}</span>
                                </p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-9">
                                <label class="form-label" for="destination">{{ __('locale.destination') }}</label>
                                <input id="destination" type="text" class="form-control input-destination" name="destination" autocomplete="destination" required autofocus data-lat="0" data-lng="0">
                            </div>
                            <div class="col-md-3">
                                <label class="form-label" for="participant">{{ __('locale.max_participant') }}</label>
                                <div class="input-group input-group-number input-group-participant" data-max-val="{{$service['participant']}}">
                                    <button class="btn btn-outline-secondary btn-minus waves-effect" type="button" disabled>
                                        <i data-feather="minus"></i>
                                    </button>
                                    <input type="text" class="form-control input-number input-hours text-center" value="1" min="1" max="{{$service['participant']}}" aria-label="Amount" required>
                                    <button class="btn btn-outline-primary btn-plus waves-effect" type="button">
                                        <i data-feather="plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-md-3">
                                <label class="form-label" for="start_time">{{ __('locale.start_time') }}</label>
                                {{-- <input id="start_time" type="text" class="form-control input-start-time" name="start_time" autocomplete="start_time" required autofocus > --}}
                                <select name="start_time" class="select2 form-control input-start-time" data-minimum-results-for-search="Infinity">
                                    @foreach($times as $key => $time)
                                        <option value="{{ Carbon\Carbon::parse($time->time)->format('H:i') }}">{{ Carbon\Carbon::parse($time->time)->format('H:i') }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="form-label" for="end_time">{{ __('locale.end_time') }}</label>
                                {{-- <input id="end_time" type="text" class="form-control input-end-time" name="end_time" autocomplete="end_time" required autofocus > --}}
                                <select name="end_time" class="select2 form-control input-end-time" data-minimum-results-for-search="Infinity">
                                    @foreach($times as $key => $time)
                                        <option value="{{ Carbon\Carbon::parse($time->time)->format('H:i') }}">{{ Carbon\Carbon::parse($time->time)->format('H:i') }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="form-label" for="hours">{{ __('locale.duration') }}</label>
                                <div class="input-group input-group-number input-group-hour" data-max-val="24">
                                    <button class="btn btn-outline-secondary btn-minus waves-effect" type="button" disabled>
                                        <i data-feather="minus"></i>
                                    </button>
                                    <input type="text" class="form-control input-number input-hours text-center" value="1" min="1" max="24" aria-label="Amount"required>
                                    <button class="btn btn-outline-primary btn-plus waves-effect" type="button">
                                        <i data-feather="plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="form-label" for="date">{{ __('locale.date') }}</label>
                                <input id="date" type="text" class="form-control input-date" name="date" autocomplete="date" required autofocus >
                            </div>
                        </div>
                        <hr>
                        <div class="row my-4">
                            <div class="col-md-3">
                                <p class="mb-25">{{ __('locale.participant') }}</p>
                                <p class="font-medium-3 font-weight-bolder text-primary sum-participant">
                                    <span class="total-participant">1</span> x Rp. {{ number_format($service['price'], 0, ',', '.') }}
                                </p>
                            </div>
                            <div class="col-md-3">
                                <p class="mb-25">{{ __('locale.duration') }}</p>
                                <p class="font-medium-3 font-weight-bolder text-primary">
                                    <span class="total-hour">1</span> x Rp. {{ number_format($service['price'], 0, ',', '.') }}
                                </p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p class="mb-25">Sub Total</p>
                                <p class="font-medium-5 font-weight-bolder text-primary">
                                    Rp. <span class="total-price">{{ number_format($service['price'], 0, ',', '.') }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-12">
                                <button class="btn btn-success w-100 btn-checkout">{{ __('locale.order') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-call-to-action mb-5 py-5" style="background-image: url('{{asset('images/banners/' . $callToAction['image'])}}');">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <div class="title mb-3" data-sal="fade" data-sal-duration="1000" data-sal-delay="">
                    <h2 class="font-large-2 font-weight-bolder text-center text-white">
                        {{ $callToAction['title'] }}
                    </h2>
                </div>
                <a href="{{ $callToAction['link'] }}" class="btn btn-light" data-sal="fade" data-sal-duration="1000" data-sal-delay="200">{{ __('landing.learn_more') }}</a>
            </div>
        </div>
    </div>
</section>

<section class="home-connect mb-5" >
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <div class="title mb-2" data-sal="slide-down" data-sal-duration="1000">
                    <h2 class="font-large-1 font-weight-bolder text-center">
                        {{ __('landing.connect_with_us') }}
                    </h2>
                </div>
                <ul class="nav-socmed p-0">
                    @foreach($socialMedia as $key => $data)
                        @if ($data->name == 'facebook')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000">
                                    <i data-feather="facebook"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'instagram')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="200">
                                    <i data-feather="instagram"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'twitter')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400">
                                    <i data-feather="twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'youtube')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="600">
                                    <i data-feather="youtube"></i>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>

<div 
  class="modal fade text-left modal-destination" 
  id="modal-destination"
  tabindex="-1"
  role="dialog"
  aria-labelledby="modal-label"
  aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-label">{{ __('locale.destination') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="map" class="w-100" style="height:300px"></div>
                <div class="form-group mt-1">
                    <label class="form-label" for="location">{{ __('locale.location_detail') }}</label>
                    <input id="location_detail" type="text" class="form-control input-location-detail" name="location_detail" autocomplete="location_detail" required autofocus>
                </div>
                <button class="btn btn-primary btn-save-location my-2">Save</button>
            </div>
        </div>
    </div>
</div>

<!--/ Page layout -->
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoGJbvqyN7FfVTw9vOAba9hLduY0o3xOU&callback=initMap&v=weekly"
      async
    ></script>
  <script src="{{ asset(('js/scripts/home/booking.js')) }}"></script>
@endsection
