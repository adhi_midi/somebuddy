@extends('layouts/indexBuddyLayoutMaster')

@section('title', 'Buddies')

@section('content')
<section class="buddies-hero mb-5" style="background-image: url('{{asset('images/banners/' . $heroBanner['image'])}}');">
    <div class="container-fluid pt-5">
        <div class="row justify-content-center">
            <div class="col-md-8" data-sal="slide-down" data-sal-duration="1000">
                <h2 class="hero-text text-white font-weight-bolder font-large-1 line-height-condensed text-center mt-3 mb-2">
                    {{ $heroBanner['title'] }}
                </h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12" data-sal="fade" data-sal-duration="1000">
                <div class="card card-search-buddy">
                    <div class="card-body">
                        <form>
                            <div class="form-row">
                                <div class="col-md-3 col-12">
                                    <select class="select2 form-control form-control-lg">
                                        @foreach($cities as $data)
                                            @if ($data->city_id == 152)
                                                <option value="{{$data->city_id}}" selected>{{$data->city_name}}</option>
                                            @else
                                                <option value="{{$data->city_id}}">{{$data->city_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 col-12">
                                    <select class="select2 form-control form-control-lg">
                                        @foreach($activities as $data)
                                            <option value="{{$data->id}}">{{$data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 col-12">
                                    <input type="text" id="fp-default" class="form-control flatpickr-basic" placeholder="date" />
                                </div>
                                <div class="col-md-2 col-12">
                                    <select class="select2 form-control form-control-lg">
                                        <option value="">Participan</option>
                                        <option value="1">0 - 5</option>
                                        <option value="1">5 - 10</option>
                                        <option value="1">10 - 20</option>
                                    </select>
                                </div>
                                <div class="col-md-1 col-12">
                                    <button class="btn btn-primary waves-effect waves-float waves-light w-100" type="submit">
                                        <i data-feather="search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="buddies-list mb-5">
    <div class="container-fluid">
        <div class="row" data-sal="slide-down" data-sal-duration="1000">
            <div class="col-6">
                <div class="title mb-2">
                    <h2 class="font-large-1 font-weight-bolder text-primary">{{ __('locale.our_buddies') }}</h2>
                </div>
            </div>
            <div class="col-6 text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-outline-primary dropdown-toggle waves-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Sort by
                    </button>
                    <div class="dropdown-menu" style="">
                      <a class="dropdown-item" href="javascript:void(0);">Name a-z</a>
                      <a class="dropdown-item" href="javascript:void(0);">Name a-z</a>
                      <a class="dropdown-item" href="javascript:void(0);">Female</a>
                      <a class="dropdown-item" href="javascript:void(0);">Male</a>
                    </div>
                  </div>
            </div>
        </div>
        <hr>
        <div class="row item-lists mt-3">
            @foreach($services as $key => $data)
                <div class="col-md-3">
                    <div class="card">
                        <div class="item-list item-buddy cursor-pointer" data-service="{{$data['user_id']}}">
                            <div class="img imgLiquidFill imgLiquid">
                                <img src="{{asset('images/profile/' . $data['user_image'])}}" alt="img-placeholder" style="display: none;">
                            </div>
                            <div class="item-content">
                                <div class="item-header mt-1">
                                    <div class="row m-0">
                                        <div class="col">
                                            <div class="badge badge-primary">{{$data['service_name']}}</div>
                                        </div>
                                        <div class="col-2 text-right">
                                            @guest
                                                <a href="javascript:void(0)" class="btn-wishlist">
                                                    <i data-feather="heart"></i>
                                                </a>
                                            @endguest
                                            @auth
                                                @if ($data['wishlist'])
                                                    <a href="javascript:void(0)" class="btn-wishlist active" data-service="{{$data['service_id']}}">
                                                        <i data-feather="heart"></i>
                                                    </a>
                                                @else
                                                    <a href="javascript:void(0)" class="btn-wishlist" data-service="{{$data['service_id']}}">
                                                        <i data-feather="heart"></i>
                                                    </a>
                                                @endif
                                            @endauth
                                        </div>
                                    </div>
                                </div>
                                <div class="item-footer position-absolute position-bottom-0 w-100 py-1">
                                    <div class="row m-0">
                                        <div class="col">
                                            <a href="{{ url('buddies/' . $data['user_id']) }}">
                                                <h3 class="font-weight-bolder text-white m-0">{{$data['user_name']}}</h3>
                                            </a>
                                            <span>
                                                <i data-feather="map-pin" class="text-white"></i>
                                                <span class="text-white ml-25 font-small-1">{{$data['location']}}</span>
                                            </span>
                                        </div>
                                        <div class="col-4 text-right pt-2">
                                            <i data-feather="star" class="text-warning"></i>
                                            <span class="font-weight-bolder text-white ml-25">{{$data['rating']}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h4 class="d-block font-weight-bolder mb-25">Rp. {{number_format($data['price'], 0, ',', '.')}}</h4>
                                    <p class="m-0">/ {{ __('locale.hour') }}</p>
                                </div>
                                <div class="col-5 text-right">
                                    <a href="{{ url('booking/' . $data['service_id']) }}" class="btn btn-primary" data-service="{{$data['service_id']}}">Book</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col text-center">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center mt-2">
                        <li class="page-item prev-item"><a class="page-link" href="javascript:void(0);"></a></li>
                        <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                        <li class="page-item" aria-current="page">
                            <a class="page-link" href="javascript:void(0);">4</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0);">5</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0);">6</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0);">7</a></li>
                        <li class="page-item next-item"><a class="page-link" href="javascript:void(0);"></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>

<section class="home-deal mb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="title mb-3">
                    <h2 class="font-large-2 font-weight-bolder text-center text-primary" data-sal="slide-down" data-sal-duration="1000">{{ __('landing.deal_of_the_week') }}</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                @foreach($banners as $key => $data)
                    @if ($data['id'] == 1)
                        @if ($data['type'] == 1)
                            <div class="item-banner item-banner-1 position-relative">
                                <div class="img imgLiquidFill imgLiquid">
                                    @if (!empty($data['image']))
                                        <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                    @else
                                        <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                    @endif
                                </div>
                                <div class="banner-body position-absolute position-bottom-0 p-2 w-100">
                                    <p class="font-medium-2 font-weight-bolder text-white mb-25">{{$data['title']}}</p>
                                    <p class="text-white">{{$data['desc']}}</p>
                                    <a href="{{$data['link']}}" class="btn btn-md btn-outline-light">Book Now</a>
                                </div>
                            </div>
                        @else
                            <div class="item-banner item-banner-1">
                                <a href="{{$data['link']}}">
                                    <div class="img imgLiquidFill imgLiquid">
                                        @if (!empty($data['image']))
                                            <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                        @else
                                            <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                        @endif
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
            <div class="col-md-6">
                @foreach($banners as $key => $data)
                    @if ($data['id'] == 2)
                        @if ($data['type'] == 1)
                            <div class="item-banner item-banner-2 position-relative">
                                <div class="img imgLiquidFill imgLiquid">
                                    @if (!empty($data['image']))
                                        <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                    @else
                                        <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                    @endif
                                </div>
                                <div class="banner-body position-absolute position-bottom-0 w-100 d-flex px-2">
                                    <div class='w-50'>
                                        <a href="{{$data['link']}}" class="btn btn-md btn-outline-light">Book Now</a>
                                    </div>
                                    <div class='w-50'>
                                        <p class="font-medium-2 font-weight-bolder text-white mb-25">{{$data['title']}}</p>
                                        <p class="text-white">{{$data['desc']}}</p>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="item-banner item-banner-2">
                                <a href="{{$data['link']}}">
                                    <div class="img imgLiquidFill imgLiquid">
                                        @if (!empty($data['image']))
                                            <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                        @else
                                            <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                        @endif
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endif

                    @if ($data['id'] == 3)
                    <div class="item-banner item-banner-3">
                        <a href="{{$data['link']}}">
                            <div class="img imgLiquidFill imgLiquid">
                                @if (!empty($data['image']))
                                    <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                @else
                                    <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                @endif
                            </div>
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</section>

<section class="home-call-to-action mb-5 py-5" style="background-image: url('{{asset('images/banners/' . $callToAction['image'])}}');">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <div class="title mb-3" data-sal="fade" data-sal-duration="1000" data-sal-delay="">
                    <h2 class="font-large-2 font-weight-bolder text-center text-white">
                        {{ $callToAction['title'] }}
                    </h2>
                </div>
                <a href="{{ $callToAction['link'] }}" class="btn btn-light" data-sal="fade" data-sal-duration="1000" data-sal-delay="200">{{ __('landing.learn_more') }}</a>
            </div>
        </div>
    </div>
</section>

<section class="home-connect mb-5" >
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <div class="title mb-2" data-sal="slide-down" data-sal-duration="1000">
                    <h2 class="font-large-1 font-weight-bolder text-center">
                        {{ __('landing.connect_with_us') }}
                    </h2>
                </div>
                <ul class="nav-socmed p-0">
                    @foreach($socialMedia as $key => $data)
                        @if ($data->name == 'facebook')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000">
                                    <i data-feather="facebook"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'instagram')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="200">
                                    <i data-feather="instagram"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'twitter')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400">
                                    <i data-feather="twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'youtube')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="600">
                                    <i data-feather="youtube"></i>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>

@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/home/buddies.js')) }}"></script>
@endsection
