@extends('layouts/indexBuddyLayoutMaster')

@section('title', 'Buddies')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/jquery.rateyo.min.css'))}}">
@endsection

@section('content')
<!-- Page layout -->
<section class="buddies-hero" style="background-image: url('{{asset('images/banners/' . $heroBanner['image'])}}');">
</section>


<section class="buddy-profile mb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="card">
                    <div class="card-body">
                        <div class="row row-up mb-3">
                            <div class="col-md-3">
                                <div class="img-profile text-center">
                                    <img class="rounded-circle box-shadow-1" src="{{asset('images/profile/' . $profile['image'])}}" alt="avatar" height="240" width="240">
                                </div>
                                <div class="info-left mt-2 pl-1">
                                    <p class="font-weight-bolder text-primary">{{ __('locale.age') }}: <span class="font-weight-bold text-secondary">{{$profile['age']}}</span></p>
                                    <p class="font-weight-bolder text-primary">{{ __('locale.gender') }}: <span class="font-weight-bold text-secondary">{{$profile['gender']}}</span></p>
                                    <p class="font-weight-bolder text-primary mb-0">{{ __('locale.language') }}:</p>
                                    <span class="font-weight-bold text-secondary">
                                        @if(!empty($profile['language']))
                                            @foreach($profile['language'] as $key => $data)
                                                @if($key == 0)
                                                    {{$data}}
                                                @else
                                                    , {{$data}}
                                                @endif
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="row mt-5">
                                    <div class="col-md-9">
                                        <div class="profile-title">
                                            <h3 class="font-large-1 font-weight-bolder text-white m-0">{{$profile['name']}}</h3>
                                            <span class="text-white">
                                                <i data-feather="map-pin"></i>
                                                <span class="font-medium-1 text-white ml-25 font-small-1 align-middle">{{$profile['location']}}</span>
                                            </span>
                                        </div>
                                        <!-- <div class="rates d-flex mt-3">
                                            <p class="font-large-1 font-weight-bolder">
                                                Rp. 100.000 / hour
                                            </p>
                                        </div> -->
                                        <div class="container-button d-md-block d-sm-none mt-3">
                                            <button class="btn btn-primary mr-50">Chat</button>
                                            <!-- <button class="btn btn-primary">Book</button> -->
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p class="font-large-1 font-weight-bolder text-primary">{{$profile['review']['rating']}} / 5</p>
                                                <div class="read-only-ratings d-inline-block mb-1 user-rating" data-rateyo-read-only="true" data-rating="{{$profile['review']['rating']}}"></div>
                                                <p class="font-medium-2 text-primary mb-0">{{$profile['review']['reviews']}} {{ __('locale.reviews') }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-button d-md-none d-sm-block mb-2">
                                    <button class="btn btn-primary mr-50">Chat</button>
                                    <!-- <button class="btn btn-primary">Book</button> -->
                                </div>
                                <hr>
                                <p class="font-medium-2 font-weight-bolder mt-2">{{ __('locale.about_me') }}</p>
                                <p>
                                    @if(!empty($profile['about']))
                                        {{ $profile['about'] }}
                                    @else
                                        -
                                    @endif
                                </p>

                                <div class="list-activity mt-2">
                                    <p class="font-weight-bolder">{{ __('locale.activities') }}</p>
                                    <div class="list-item">
                                        @if(!empty($profile['activity']))
                                            @foreach($profile['activity'] as $key => $data)
                                                <div class="btn btn-md btn-outline-primary disabled round mb-50">{{$data}}</div>
                                            @endforeach
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>

                                <div class="list-activity mt-2">
                                    <p class="font-weight-bolder">{{ __('locale.interest') }}</p>
                                    <div class="list-item">
                                        @if(!empty($profile['interest']))
                                            @foreach($profile['interest'] as $key => $data)
                                                <div class="btn btn-md btn-outline-primary disabled round mb-50">{{$data}}</div>
                                            @endforeach
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-center mb-2 mt-1">
                            <p class="font-medium-3">{{ __('locale.activities') }}</p>
                            <p class="font-large-1 font-weight-bolder text-primary">{{ __('locale.offered_activity') }}</p>
                            </div>
                        </div>
                        <div class="row services">
                            @foreach($services as $key => $data)
                                <div class="col-md-4">
                                    <div class="card border">
                                        <div class="card-body d-flex">
                                            <div class="w-75">
                                                <p class="font-medium-3 font-weight-bolder text-primary mb-25">
                                                    {{$data['name']}}
                                                </p>
                                                <p class="font-medium-1 font-weight-bolder mb-25">
                                                    Rp. {{$data['price']}}/jam
                                                </p>
                                            </div>
                                            <div class="w-25">
                                                <a href="{{ url('booking/' . $data['id']) }}" class="btn btn-primary" data-service="{{$data['id']}}">Book</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-center mb-2 mt-1">
                            <p class="font-medium-3">{{ __('locale.gallery') }}</p>
                            <p class="font-large-1 font-weight-bolder text-primary">{{ __('locale.my_special_moment') }}</p>
                            </div>
                        </div>
                        <div class="row galleries mb-3">
                            @if (!empty($galleries))
                                @foreach($galleries as $key => $data)
                                <div class="col-md-2">
                                    <a href="{{asset('images/gallery/' . $data['image'])}}" data-fslightbox>
                                        <div class="img imgLiquidFill imgLiquid item-gallery mb-2">
                                        <img src="{{asset('images/gallery/' . $data['image'])}}" alt="img-placeholder">
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                            @else
                                <div class="col-md-12 text-center">
                                    <div class="divider w-25 d-inline-block">
                                        <div class="divider-text">{{ __('locale.gallery_empty') }}</div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-center mb-2 mt-1">
                            <p class="font-medium-3">{{ __('locale.reviews') }}</p>
                            <p class="font-large-1 font-weight-bolder text-primary">{{ __('locale.what_my_buddy_say') }}</p>
                            </div>
                        </div>
                        <div class="row review">
                            @if (!empty($reviews))
                                <div class="col-md-12 slick" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>
                                    @foreach($reviews as $data)
                                        <div class="item-testimonial text-center px-2" data-sal="fade" data-sal-duration="1000">
                                            <span class="avatar mb-1">
                                                <img class="round" src="{{asset('images/profile/thumb/' . $data['image'])}}" alt="avatar" height="100" width="100">
                                            </span>
                                            <h4 class="font-weight-bolder text-primary m-0">{{$data['name']}}</h4>
                                            <p class="font-weight-bolder">{{$data['age']}}th</p>
                                            <p class="font-weight-bolder">
                                                <span class="text-primary">{{$data['service']}} </span>
                                                - 
                                                <i data-feather="star" class="text-warning"></i>
                                                {{$data['rating']}}
                                            </p>
                                            <p>{{$data['desc']}}</p>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                <div class="col-md-12">
                                    <div class="col-md-12 text-center">
                                        <div class="divider w-25 d-inline-block">
                                            <div class="divider-text">{{ __('locale.reviews_empty') }}</div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-call-to-action mb-5 py-5" style="background-image: url('{{asset('images/banners/' . $callToAction['image'])}}');">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <div class="title mb-3" data-sal="fade" data-sal-duration="1000" data-sal-delay="">
                    <h2 class="font-large-2 font-weight-bolder text-center text-white">
                        {{ $callToAction['title'] }}
                    </h2>
                </div>
                <a href="{{ $callToAction['link'] }}" class="btn btn-light" data-sal="fade" data-sal-duration="1000" data-sal-delay="200">{{ __('landing.learn_more') }}</a>
            </div>
        </div>
    </div>
</section>

<section class="home-connect mb-5" >
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <div class="title mb-2" data-sal="slide-down" data-sal-duration="1000">
                    <h2 class="font-large-1 font-weight-bolder text-center">
                        {{ __('landing.connect_with_us') }}
                    </h2>
                </div>
                <ul class="nav-socmed p-0">
                    @foreach($socialMedia as $key => $data)
                        @if ($data->name == 'facebook')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000">
                                    <i data-feather="facebook"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'instagram')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="200">
                                    <i data-feather="instagram"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'twitter')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400">
                                    <i data-feather="twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'youtube')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="600">
                                    <i data-feather="youtube"></i>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>

<!--/ Page layout -->
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/extensions/jquery.rateyo.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/home/buddy-detail.js')) }}"></script>
@endsection
