@extends('layouts/indexLayoutMaster')

@section('title', 'Somebuddy')

@section('content')
<section class="home-hero" style="background-image: url('{{asset('images/banners/' . $heroBanner['image'])}}');">
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-md-6">
                <h2 class="hero-text text-white font-weight-bolder font-large-2 line-height-condensed mt-5 text-center text-md-left" data-sal="slide-right" data-sal-duration="1000" data-lang="{{ app()->getLocale() }}" >
                    {{ $heroBanner['title'] }}
                </h2>
            </div>
        </div>
    </div>
</section>

<section class="home-activities mb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="list-activity" data-sal="slide-down" data-sal-duration="1000">
                    @foreach($activities as $key => $data)
                        <div class="item-activity text-center d-inline-block float-left">
                            <div class="d-inline-block item-image mb-1">
                                @if (!empty($data['image']))
                                    <img src="{{asset('images/icon/' . $data['image'])}}"/>
                                @else
                                    <img src="{{asset('images/icon/somebuddy-logo.png')}}"/>
                                @endif
                            </div>
                            <div class="item-detail ">
                                <p class="title font-medium-3 font-weight-bolder text-primary mb-25">
                                    {{ $data['title']}}
                                </p>
                                <p class="desc font-small-3 text-primary">
                                    {{ $data['desc']}}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12 text-center mt-2">
                <button class="btn btn-outline-secondary round" data-sal="fade" data-sal-duration="1000" data-sal-delay="200">{{ __('landing.activity_more') }}</button>
            </div>
        </div>
    </div>
</section>

<section class="home-about-us mb-5 py-4" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6" data-sal="slide-right" data-sal-duration="1000">
                <p class="font-large-2 font-weight-bolder text-primary">{{ __('landing.why_choose_us') }}</p>
                <h3 class="font-large-2 font-weight-bolder">{{ $aboutUs['title'] }}</h3>
            </div>
            <div class="col-md-6">
                <div class="row m-0 list-about">
                    @foreach($aboutUs['about_us_item'] as $data)
                    <div class="col-md-4">
                        <div class="item-about text-center">
                            <div class="d-inline-block item-image mb-1">
                                @if (!empty($data['image']))
                                    <img src="{{asset('images/icon/' . $data['image'])}}"/>
                                @else
                                    <img src="{{asset('images/icon/somebuddy-logo.png')}}"/>
                                @endif
                            </div>
                            <div class="item-detail ">
                                <p class="desc font-weight-bolder font-small-3 text-primary">
                                    {{ $data['title']}}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-12">
                <p class="text-primary">{{ $aboutUs['desc'] }}</p>
            </div>
        </div>
    </div>
</section>

<section class="home-buddy mb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="title mb-3" data-sal="slide-down" data-sal-duration="1000">
                    <h2 class="font-large-2 font-weight-bolder text-center text-white">{{ __('landing.our_buddies') }}</h2>
                    <h3 class="font-weight-bolder text-center text-white">{{ __('landing.best_recommendation') }}</h2>
                </div>
            </div>
        </div>
        
        <div class="row item-lists" data-sal="slide-up" data-sal-duration="1000">
            @foreach($buddies as $key => $data)
                <div class="col-md-3">
                    <div class="card">
                        <div class="item-list item-buddy cursor-pointer" data-service="{{$data['user_id']}}">
                            <a href="">
                                <div class="img imgLiquidFill imgLiquid">
                                    <img src="{{asset('images/profile/' . $data['user_image'])}}" alt="img-placeholder" style="display: none;">
                                </div>
                            </a>
                            <div class="item-content">
                                <div class="item-header mt-1">
                                    <div class="row m-0">
                                        <div class="col">
                                            <div class="badge badge-primary">{{$data['service_name']}}</div>
                                        </div>
                                        <div class="col-2 text-right">
                                            @guest
                                                <a href="javascript:void(0)" class="btn-wishlist">
                                                    <i data-feather="heart"></i>
                                                </a>
                                            @endguest
                                            @auth
                                                @if ($data['wishlist'])
                                                    <a href="javascript:void(0)" class="btn-wishlist active" data-service="{{$data['service_id']}}">
                                                        <i data-feather="heart"></i>
                                                    </a>
                                                @else
                                                    <a href="javascript:void(0)" class="btn-wishlist" data-service="{{$data['service_id']}}">
                                                        <i data-feather="heart"></i>
                                                    </a>
                                                @endif
                                            @endauth
                                        </div>
                                    </div>
                                </div>
                                <div class="item-footer position-absolute position-bottom-0 w-100 py-1">
                                    <div class="row m-0">
                                        <div class="col">
                                            <a href="{{ url('buddies/' . $data['user_id']) }}">
                                                <h3 class="font-weight-bolder text-white m-0">{{$data['user_name']}}</h3>
                                            </a>
                                            <span>
                                                <i data-feather="map-pin" class="text-white"></i>
                                                <span class="text-white ml-25 font-small-1">{{$data['location']}}</span>
                                            </span>
                                        </div>
                                        <div class="col-4 text-right pt-2">
                                            <i data-feather="star" class="text-warning"></i>
                                            <span class="font-weight-bolder text-white ml-25">{{$data['rating']}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h4 class="d-block font-weight-bolder">Rp. {{number_format($data['price'], 0, ',', '.')}}</h4>
                                    <p class="m-0">/ {{ __('locale.hour') }}</p>
                                </div>
                                <div class="col-5 text-right">
                                    <a href="{{ url('booking/' . $data['service_id']) }}" class="btn btn-primary" data-service="{{$data['service_id']}}">Book</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row" data-sal="slide-up" data-sal-duration="1000">
            <div class="col text-center">
                <a href="{!! url('/buddies'); !!}" class="btn btn-light">{{ __('locale.view_more') }}</a>
            </div>
        </div>
    </div>
</section>

<section class="home-deal mb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="title mb-3">
                    <h2 class="font-large-2 font-weight-bolder text-center text-primary" data-sal="slide-down" data-sal-duration="1000">{{ __('landing.deal_of_the_week') }}</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                @foreach($banners as $key => $data)
                    @if ($data['id'] == 1)
                        @if ($data['type'] == 1)
                            <div class="item-banner item-banner-1 position-relative">
                                <div class="img imgLiquidFill imgLiquid">
                                    @if (!empty($data['image']))
                                        <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                    @else
                                        <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                    @endif
                                </div>
                                <div class="banner-body position-absolute position-bottom-0 p-2 w-100">
                                    <p class="font-medium-2 font-weight-bolder text-white mb-25">{{$data['title']}}</p>
                                    <p class="text-white">{{$data['desc']}}</p>
                                    <a href="{{$data['link']}}" class="btn btn-md btn-outline-light">Book Now</a>
                                </div>
                            </div>
                        @else
                            <div class="item-banner item-banner-1">
                                <a href="{{$data['link']}}">
                                    <div class="img imgLiquidFill imgLiquid">
                                        @if (!empty($data['image']))
                                            <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                        @else
                                            <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                        @endif
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
            <div class="col-md-6">
                @foreach($banners as $key => $data)
                    @if ($data['id'] == 2)
                        @if ($data['type'] == 1)
                            <div class="item-banner item-banner-2 position-relative">
                                <div class="img imgLiquidFill imgLiquid">
                                    @if (!empty($data['image']))
                                        <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                    @else
                                        <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                    @endif
                                </div>
                                <div class="banner-body position-absolute position-bottom-0 w-100 d-flex px-2">
                                    <div class='w-50'>
                                        <a href="{{$data['link']}}" class="btn btn-md btn-outline-light">Book Now</a>
                                    </div>
                                    <div class='w-50'>
                                        <p class="font-medium-2 font-weight-bolder text-white mb-25">{{$data['title']}}</p>
                                        <p class="text-white">{{$data['desc']}}</p>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="item-banner item-banner-2">
                                <a href="{{$data['link']}}">
                                    <div class="img imgLiquidFill imgLiquid">
                                        @if (!empty($data['image']))
                                            <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                        @else
                                            <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                        @endif
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endif

                    @if ($data['id'] == 3)
                    <div class="item-banner item-banner-3">
                        <a href="{{$data['link']}}">
                            <div class="img imgLiquidFill imgLiquid">
                                @if (!empty($data['image']))
                                    <img src="{{asset('images/banners/' . $data['image'])}}"/>
                                @else
                                    <img src="{{asset('images/banners/banner-default.jpg')}}"/>
                                @endif
                            </div>
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</section>

<section class="home-testimonial mb-5 py-4" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="title mb-5" data-sal="slide-down" data-sal-duration="1000">
                    <h2 class="font-large-2 font-weight-bolder text-center text-primary">{{ __('landing.what_they_say') }}</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 slick" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>
                @foreach($testimonials as $data)
                <div class="item-testimonial text-center px-2" data-sal="fade" data-sal-duration="1000">
                    <span class="avatar mb-1">
                        <img class="round" src="{{asset('' . $data['image'])}}" alt="avatar" height="100" width="100">
                    </span>
                    <h4 class="font-weight-bolder text-primary m-0">{{$data['name']}}</h4>
                    <p class="font-weight-bolder">{{$data['age']}}th</p>
                    <p>{{$data['desc']}}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<section class="home-call-to-action mb-5 py-5" style="background-image: url('{{asset('images/banners/' . $callToAction['image'])}}');">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <div class="title mb-3" data-sal="fade" data-sal-duration="1000" data-sal-delay="">
                    <h2 class="font-large-2 font-weight-bolder text-center text-white">
                        {{ $callToAction['title'] }}
                    </h2>
                </div>
                <a href="{{ $callToAction['link'] }}" class="btn btn-light" data-sal="fade" data-sal-duration="1000" data-sal-delay="200">{{ __('landing.learn_more') }}</a>
            </div>
        </div>
    </div>
</section>

<section class="home-connect mb-5" >
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <div class="title mb-2" data-sal="slide-down" data-sal-duration="1000">
                    <h2 class="font-large-1 font-weight-bolder text-center">
                        {{ __('landing.connect_with_us') }}
                    </h2>
                </div>
                <ul class="nav-socmed p-0">
                    @foreach($socialMedia as $key => $data)
                        @if ($data->name == 'facebook')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000">
                                    <i data-feather="facebook"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'instagram')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="200">
                                    <i data-feather="instagram"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'twitter')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400">
                                    <i data-feather="twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if ($data->name == 'youtube')
                            <li class="d-inline-block">
                                <a class="nav-link" href="{{$data->link}}" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="600">
                                    <i data-feather="youtube"></i>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>

<!--/ Page layout -->
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(('js/scripts/home/index.js')) }}"></script>
@endsection
