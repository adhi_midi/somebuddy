@php
$configData = Helper::applClasses();
@endphp
@extends('layouts/fullLayoutMaster')

@section('title', 'Register Page')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
<div class="auth-wrapper auth-v2">
  <div class="auth-inner row m-0">
    <!-- Brand logo-->
    <a class="brand-logo" href="/">
      <img src="{{asset('images/logo/logo.png')}}" height="40" width="40">
      <h2 class="brand-text text-white ml-1">Somebuddy</h2>
    </a>
    <!-- /Brand logo-->
    <!-- Left Text-->
    <div class="d-none d-lg-flex col-lg-6 align-items-center p-5 imgLiquidFill imgLiquid">
      <img src="{{asset('images/banners/hero-banner.jpg')}}" />
    </div>
      <!-- /Left Text-->
      <!-- Register-->
    <div class="d-flex col-lg-6 align-items-center auth-bg px-2 p-lg-5">
      <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
        <h2 class="card-title font-weight-bold mb-1">Register</h2>
        <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
            @csrf
          <div class="form-group row">
            <div class="col-md-6">
                <label class="form-label" for="register-username">Name</label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="col-md-6">
                <label class="form-label" for="register-email">Email</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
          </div>

          <div class="form-group row">
              <div class="col-md-6">
                <label class="form-label" for="register-email">Address</label>
                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>

                @error('address')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="col-md-6">
              <label class="form-label" for="register-email">Phone Number</label>
                    <input id="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus>

                    @error('phone_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
              </div>
          </div>
        
          <div class="form-group row">
              <div class="col-md-6">
                <label class="form-label" for="register-password">Password</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="col-md-6">
                <label class="form-label" for="register-password">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
              </div>
          </div>

          <div class="form-group row">
              <div class="col-md-6">
                <label class="form-label">Gender</label>
                <select name="gender" class="form-select form-control"> 
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
              </div>
          </div>

          <div class="form-group row">
            <div class="col-md-6">
                <label class="form-label">Image Identity</label>
                <input id="image_identity" type="file" class="form-control @error('image_identity') is-invalid @enderror" name="image_identity" value="{{ old('image_identity') }}" required autocomplete="image_identity" autofocus>

                @error('image_identity')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

            </div>
            <div class="col-md-6">
                <label class="form-label">Image Selfie</label>
                <input id="image_selfie" type="file" class="form-control @error('image_selfie') is-invalid @enderror" name="image_selfie" value="{{ old('image_selfie') }}" required autocomplete="image_selfie" autofocus>

                @error('image_selfie')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
          </div>

          <div class="form-group">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input" id="register-privacy-policy" type="checkbox" tabindex="4" />
              <label class="custom-control-label" for="register-privacy-policy">I agree to<a href="javascript:void(0);">&nbsp;privacy policy & terms</a></label>
            </div>
          </div>
          <button class="btn btn-primary btn-block" tabindex="5" type="submit" >Sign up</button>
        </form>
        <p class="text-center mt-2">
          <span>Already have an account?</span>
          <a href="{{url('login')}}"><span>&nbsp;Sign in instead</span></a>
        </p>
      </div>
    </div>
  <!-- /Register-->
  </div>
</div>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('js/scripts/pages/page-auth-register.js')}}"></script>
@endsection

