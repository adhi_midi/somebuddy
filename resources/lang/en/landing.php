<?php

return [ 
  "hero_home" => "We Help You to Find The Right Trainer for Your Activity",
  "hero_line_1" => "We Help You to Find",
  "hero_line_2" => "The Right Trainer for Your Activity",
  "deal_of_the_week" => "Deal Of The Week",
  "our_buddies" => "Our Buddies",
  "best_recommendation" => "The Best Recommendation",
  "why_choose_us" => "Why Choose Us",
  "about_us" => "About Us",
  "about_us_title" => "We Make All The Process Easy",
  "about_us_desc" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus elementum, lacus vitae tristique convallis, leo felis dapibus lectus, in egestas elit dui eget quam",
  "what_they_say" => "What They Say About Somebuddy",
  "call_to_action" => "Dengan menjadi partner Somebuddy dapatkan penghasilan tambahan dengan cara yang mudah",
  "learn_more" => "Learn More",
  "connect_with_us" => "Connect with Us",
  "activity_more" => "Find More",
];
