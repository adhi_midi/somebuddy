<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\BuddiesController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\Member\BookingContoller;
use App\Http\Controllers\Member\CheckoutContoller;
use App\Http\Controllers\Member\DashboardController;
use App\Http\Controllers\Member\InboxController;
use App\Http\Controllers\Member\ResolutionController;
use App\Http\Controllers\Member\ReviewController;
use App\Http\Controllers\Member\ScheduleController;
use App\Http\Controllers\Member\ServiceController;
use App\Http\Controllers\Member\GalleryController;
use App\Http\Controllers\Member\BankController;
use App\Http\Controllers\Member\SettingsController;
use App\Http\Controllers\Member\TransactionController;
use App\Http\Controllers\Member\WalletController;
use App\Http\Controllers\Member\WishlistController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'buddies'], function () {
    Route::get('/', [BuddiesController::class, 'index'])->name('buddies');
    Route::get('/{id}', [BuddiesController::class, 'detail'])->name('buddy-detail');
    Route::get('/page/{id}', [BuddiesController::class, 'page'])->name('buddies-page');
});

Route::get('login', [AuthController::class, 'showLoginForm'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('register', [RegisterController::class, 'create']);

Route::group(['middleware' => 'auth'], function () {

    Route::get('signout', [AuthenticationController::class, 'signout'])->name('signout');
    
    Route::get('/booking/{id}', [BookingContoller::class, 'index'])->name('booking');
    Route::post('/booking/store', [BookingContoller::class, 'store'])->name('booking-store');
    Route::get('/checkout', [BookingContoller::class, 'checkout'])->name('checkout-index');
    Route::post('/checkout/{id}', [BookingContoller::class, 'checkoutStore'])->name('checkout-store');
    Route::get('/checkout/payment/{id}', [BookingContoller::class, 'paymentDetail'])->name('payment-detail');

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', [DashboardController::class,'index'])->name('dashboard');
    });

    Route::group(['prefix' => 'inbox'], function () {
        Route::get('/', [InboxController::class,'index'])->name('inbox');
        Route::get('/{id}', [InboxController::class,'detail'])->name('inbox-detail');
        Route::post('/{id}/update', [InboxController::class,'update'])->name('inbox-update');
    });

    Route::group(['prefix' => 'transaction'], function () {
        Route::get('/', [TransactionController::class,'index'])->name('transaction');
        Route::post('/{id}', [TransactionController::class,'getTransactions'])->name('transaction-type');
        Route::post('/add', [TransactionController::class,'store'])->name('transaction-add');
        Route::post('/update', [TransactionController::class,'update'])->name('transaction-update');
    });

    Route::group(['prefix' => 'wallet'], function () {
        Route::get('/', [WalletController::class,'index'])->name('wallet');
        Route::get('/deposit', [WalletController::class,'deposit'])->name('wallet-deposit');
        Route::post('/deposit/store', [WalletController::class,'depositStore'])->name('wallet-deposit-store');
        Route::get('/withdraw', [WalletController::class,'withdraw'])->name('wallet-withdraw');
        Route::post('/withdraw/store', [WalletController::class,'withdrawStore'])->name('wallet-withdraw-store');
        Route::get('/bank', [WalletController::class,'bank'])->name('wallet-bank');
        Route::post('/bank/store', [WalletController::class,'bankStore'])->name('wallet-bank-store');
    });

    Route::group(['prefix' => 'resolution'], function () {
        Route::get('/', [ResolutionController::class,'index'])->name('resolution');
        Route::get('/{id}', [ResolutionController::class,'detail'])->name('resolution-detail');
        Route::post('/{id}/update', [ResolutionController::class,'update'])->name('resolution-update');
    });

    Route::group(['prefix' => 'review'], function () {
        Route::get('/', [ReviewController::class,'index'])->name('review');
        Route::post('/add', [ReviewController::class,'store'])->name('review-add');
        Route::post('/update', [ReviewController::class,'update'])->name('review-update');
    });

    Route::group(['prefix' => 'wishlist'], function () {
        Route::get('/', [WishlistController::class,'index'])->name('wishlist');
        Route::post('/add', [WishlistController::class,'store'])->name('wishlist-add');
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', [SettingsController::class,'profile']);
        Route::get('/profile', [SettingsController::class,'profile'])->name('profile-setting');
        Route::post('/profile/update', [SettingsController::class,'updateProfile'])->name('update-profile');
        Route::post('/profile/update-image', [SettingsController::class,'updateProfileImage'])->name('update-profile-image');
        Route::post('/profile/update-password', [SettingsController::class,'updatePassword'])->name('update-password');
        Route::post('/upgrade', [SettingsController::class,'upgradeMember']);
        Route::get('/upgrade/{id}', [SettingsController::class,'upgradeApprove']);
        
        Route::post('/gallery/add', [GalleryController::class,'store'])->name('gallery-add');
        Route::post('/gallery/update', [GalleryController::class,'update'])->name('gallery-update');
        
        Route::get('/banks', [BankController::class,'index'])->name('bank-setting');
        Route::post('/banks/add', [BankController::class,'store'])->name('bank-add');
        Route::post('/banks/{id}/update', [BankController::class,'update'])->name('bank-update');
        
        Route::get('/services', [ServiceController::class,'index'])->name('service-setting');
        Route::get('/services/create', [ServiceController::class,'create'])->name('service-create');
        Route::get('/services/{id}', [ServiceController::class,'detail'])->name('service-detail');
        Route::post('/services/{id}/delete', [ServiceController::class,'delete'])->name('service-delete');
        Route::get('/services/{id}/schedules', [ServiceController::class,'getServiceSchedules'])->name('get-service-schedules');
        Route::post('/services/schedules/{id}/update', [ServiceController::class,'updateServiceSchedule'])->name('update-service-schedule');
        Route::post('/services/schedules/{id}/delete', [ServiceController::class,'deleteServiceSchedule'])->name('delete-service-schedule');
        Route::post('/services/add', [ServiceController::class,'store'])->name('service-add');
        Route::post('/services/{id}/update', [ServiceController::class,'update'])->name('service-update');
    });

    Route::group(['prefix' => 'schedule'], function () {
        Route::get('/', [ScheduleController::class,'index'])->name('schedule');
        Route::post('/update', [ScheduleController::class,'update'])->name('schedule-update');
    });
    
});

// locale Route
Route::get('lang/{locale}', [LanguageController::class, 'swap']);

Auth::routes();
